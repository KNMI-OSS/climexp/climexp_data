#!/bin/sh
# make annual derived data such as RXnday from the EC-Earth daily data
vars="tx3x rx1day rx3day rx5day txx txn tnx tnn"
for var in $vars; do
    case $var in
        rx1day) invar=pr;args="max";;
        rx3day) invar=pr;args="max sum 3";;
        rx5day) invar=pr;args="max sum 5";;
        txx) invar=tasmax;args="max";;
        tx3x) invar=tasmax;args="max ave 3";;
        txn) invar=tasmax;args="min";;
        tnx) invar=tasmin;args="max";;
        tnn) invar=tasmin;args="min";;
        *) echo "$0: error: please give definition of $var";exit -1;;
    esac

    for scen in historical historicalNat; do
        r=0
        while [ $r -lt 15 ]; do
            r=$((r+1))
            indir=$invar
            outdir=$var
            mkdir -p $outdir
            infiles=$indir/${invar}_day_HadGEM3-A-N216_${scen}_r${r}i1p1_????????-????????.nc
            infile=`ls $infiles|head -1`
            if [ ! -s "$infile" ]; then
                infiles=$indir/${invar}_day_HadGEM3-A-N216_${scen}_r1i1p${r}_????????-????????.nc
            fi
            outfiles=""
            varfile=${var}_yr_HadGEM3-A-N216_${scen}_r${r}i1p1_196001-201312.nc
            varfile=$outdir/$varfile
            if [ ! -s $varfile ]; then
                for infile in $infiles; do
                    outfile=/tmp/${var}_`basename $infile`
                    outfiles="$outfiles $outfile"
                    if [ ! -s $outfile -o $outfile -ot $infile ]; then
                        echo "daily2longerfield $infile 1 $args $outfile"
                        daily2longerfield $infile 1 $args $outfile
                        if [ $? != 0 -o ! -s $outfile ]; then
                            echo "something went wrong"
                            exit -1
                        fi
                    fi
                done
                echo "cdo -r -f nc4 -z zip copy $outfiles $varfile" 
                cdo -r -f nc4 -z zip copy $outfiles $varfile
                rm $outfiles
            fi
        done
        
#       short runs

        i=0
        p=0
        while [ $p -lt 7 ]; do
            ((p++))
            r=0
            while [ $r -lt 15 ]; do
                r=$((r+1))
                indir=${invar}_short
                outdir=${var}_short
                mkdir -p $outdir
                infile=$indir/${invar}_day_HadGEM3-A-N216_${scen}Short_r${r}i1p${p}_20140101-20151230.nc
                outfile=${var}_yr_HadGEM3-A-N216_${scen}_r${r}i1p1_201401-201512.nc
                outfile=$outdir/$outfile
                if [ ! -s $outfile ]; then
                    echo "daily2longerfield $infile 1 $args $outfile"
                    daily2longerfield $infile 1 $args $outfile
                    if [ $? != 0 -o ! -s $outfile ]; then
                        echo "something went wrong"
                        exit -1
                    fi
                fi
                linkdir=annual/$var
                ii=`printf %03i $i`
                linkfile=$linkdir/${var}_yr_HadGEM3-A-N216_${scen}_$ii.nc
                [ -e $linkfile ] && rm $linkfile
                if [ $p = 1 ]; then
                    longfile=$var/${var}_yr_HadGEM3-A-N216_${scen}_r${r}i1p1_196001-201312.nc
                    cdo -r -f nc4 -z zip copy $longfile $outfile $linkfile
                else
                    cp $outfile $linkfile
                fi
                ((i++))
            done
        done
    done # scen
done # var
