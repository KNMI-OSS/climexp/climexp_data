CHIRPS, CPC, MSWEP, stations via Clair Barnes, c.barnes22@imperial.ac.uk
CPC, ERA5 tas via Mariam Zachariah, m.zachariah@imperial.ac.uk
UKCP18 via Clair Barnes, c.barnes22@imperial.ac.uk
CORDEX via Clair Barnes, c.barnes22@imperial.ac.uk
HighResMIP via Clair Barnes, c.barnes22@imperial.ac.uk
CMIP6 SGSAT via Clair Barnes, c.barnes22@imperial.ac.uk
FLOR and AM2.5C60 via Wenchang Yang, wenchang@princeton.edu
CMIP6 via Robert Vautard, rvautard@ipsl.fr