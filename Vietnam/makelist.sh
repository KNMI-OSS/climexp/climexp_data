#!/bin/bash
i=0
for file in ncfiles/R_daily_*.nc; do
    ((i++))
    id=`printf %02i $i`
    name=`basename $file .nc`
    name=${name#R_daily_}
    f=rr${id}_${name}.nc
    if [ ! -s $f ]; then
        cp $file $f
        ncatted -a units,lat,m,c,"degrees_north" -a units,lon,m,c,"degrees_east" \
            -a standard_name,prcp,m,c,"precipitation" $f
    fi
done

for file in rr??_*.nc; do
    f=${file%%_*.nc}.nc
    if [ ! -L $f ]; then
        ln -s $file $f
    fi
    datfile=${f%.nc}.dat
    if [ ! -s $datfile ]; then
        netcdf2dat $f > $datfile
    fi
    gzfile=${f%.nc}.gz
    if [ ! -s $gzfile ]; then
        gzip -c $datfile > $gzfile
    fi
done

cat > list_rr_vietnam.txt <<EOF
located stations in 8.0N:24.0N, 100.0E:112.0E
==============================================
EOF

for file in rr??_*.nc; do
    id=${file%%_*}
    id=${id#rr}
    name=${file#rr??_}
    name=${name%.nc}
    lon=`ncdump -c $file | fgrep -v 'lon = 1 ' | fgrep 'lon = ' | sed -e 's/ *lon = //' -e 's/ *; *//'`
    lat=`ncdump -c $file | fgrep -v 'lat = 1 ' | fgrep 'lat = ' | sed -e 's/ *lat = //' -e 's/ *; *//'`
    firstyr=`firstvalid $file | cut -b 2-5`
    lastyr=`lastvalid $file | cut -b 2-5`
    nyr=$((lastyr - firstyr + 1))

    cat >> list_rr_vietnam.txt <<EOF
$name (Vietnam)
coordinates: ${lat}N, ${lon}E
station code: $id $name
Found $nyr years of data in ${firstyr}-${lastyr}
==============================================
EOF
done