#!/bin/bash
wget -q -N https://en.wikipedia.org/wiki/Atlantic_hurricane_season
egrep -e 'season">18[5-9][0-9]<|season">19[0-9][0-9]<|season">20[0-9][0-9]<|^<td>[0-9]$|<td>[0-9][0-9]$' \
    Atlantic_hurricane_season | fgrep -v ' a ' | fgrep -v '1850' | \
    sed -e 's/^<td><a.* season">/@/' -e 's@</a>@@' -e 's/<td>//' | \
    tr '\n' ' ' | tr '@' '\n' | egrep -v '^ *$' > aap.txt
for type in ns hu ih; do
    case $type in
        ns) col=2; name="number of Atlantic named storms";;
        hu) col=3; name="number of Atlantic hurricanes";;
        ih) col=4; name="number of Atlantic major hurricanes";;
        ace) col=5; name="Atlantic Accumulated Cyclone Energy";;
        *) echo "errror mnbvcgfew";exit -1;;
    esac
    cat > atlantic_$type.dat <<EOF
# $type [1] $name
# from https://en.wikipedia.org/wiki/Atlantic_hurricane_season
# institution :: AOML
# source :: https://en.wikipedia.org/wiki/Atlantic_hurricane_season
# generated `date`
EOF
        egrep '^18|^19[0-7]' aap.txt | awk "{print \$1 \" \" \$$col}" >> atlantic_$type.dat
        egrep '^19[8-9]|^2[0-9]' aap.txt | awk "{print \$1 \" \" \$$((col+1))}" >> atlantic_$type.dat
done
