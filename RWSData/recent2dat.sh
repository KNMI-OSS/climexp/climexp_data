#!/bin/sh
# get CVS file by hand from https://waterinfo.rws.nl
file="$1"
###echo "Save with unix linefeeds..."
###edit $file
f=${file%.csv}_hr.csv
fgrep ':00:00;' $file > $f
c1=`fgrep -c Eijsden $f`
c2=`fgrep -c Lobith $f`
if [ $c1 != 0 ]; then
    place=eijsden
    name="Meuse at Eijsden"
elif [ $c2 != 0 ]; then
    place=lobith
    name="Rhine at Lobith"
fi
temp=${f%.csv}.dat
cat > $temp <<EOF
# recent daily discharge data from Rijkswaterstaat
# history :: `date` downloaded $file from https://waterwebservices.rijkswaterstaat.nl 
# discharge [m^3/s] discharge of the $name
# daily means of hourly observations.
# institution :: Rijkswaterstaat
EOF
cut -d ';' -f 22,23,26 $f | tr ':;-' '   ' | awk '{print $3 $2 $1 $4 " " $7}' | tr ',' '.' | fgrep -v '999999' >> $temp
daily2longer $temp 366 mean > debiet_${place}_recent.dat

hist=debiet_$place.dat
if [ ! -s $hist ]; then
    echo "$0: error: cannot find $hist"
    exit -1
fi
hist1=debiet_${place}_2017_2019.dat
if [ ! -s $hist1 ]; then
    echo "$0: error: cannot find $hist1"
    exit -1
fi
patchseries $hist $hist1 > debiet_${place}_1.dat
patchseries debiet_${place}_1.dat debiet_${place}_recent.dat > debiet_${place}_ext.dat
if [ $place = eijsden ]; then
    oldfile=mons.dat
    if [ ! -s mons.dat ]; then
        ./meuse_txt2dat.sh
    fi
    patchseries debiet_${place}_ext.dat mons.dat > debiet_monsin_${place}_ext.dat
fi
