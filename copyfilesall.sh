#!/bin/sh
files="$@"
if [ -z "$files" ]; then
  echo "usage: $0 files"
  echo "       copies files to Climate Explorer, ganesha and gatotkaca"
fi
cwd=`pwd`
dir=`basename $cwd`
[ -z "$HOST" ] && HOST=`hostname`
if [ "$HOST" = pc200270.knmi.nl ]; then
  rsync -e ssh -at --timeout=60 "$@" climexp.knmi.nl:climexp/$dir/
  rsync -e ssh -at --timeout=60 "$@" climexp-test.duckdns.org:climexp/$dir/
  rsync -e 'ssh -p 23' -at --timeout=20 "$@" gj@ganesha.xs4all.nl:NINO/$dir/
  ip=`host gatotkaca.duckdns.org | fgrep -v mail | fgrep -v "not found" | sed -e 's/.*address //'`
  if [ "${ip#145\.23}" != "$ip" ]; then
  # gatotkaca while I am working at KNMI
    rsync -e ssh -at --timeout=20 "$@" gj@gatotkaca.duckdns.org:NINO/$dir/
  elif [ "${ip#192\.168\.178}" != "$ip" ]; then
  # goes to gatotkaca as well while I am working from home
    rsync -e ssh -at --timeout=20 "$@" gj@ganesha.xs4all.nl:NINO/$dir/
  fi
fi

