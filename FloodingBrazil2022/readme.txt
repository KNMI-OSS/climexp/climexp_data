data for attribution analysis FloodingBrazil2022 (May-June 2022) on precipitation and flooding North East Coast Brazil
stations data from Francisco das Chagas Vasconcelos Junior juniorphy@gmail.com via Mariam Zachariam m.zachariah@imperial.ac.uk
AMIP and FLOR data provided by Wenchang Yang wenchang@princeton.edu
HighResMIP data SST forced provided by Sihan Li sihan.li@ouce.ox.ac.uk
CORDEX data provided by Remy Bonnet rbonnet@ipsl.fr
