data for attribution analysis KZNfloods2022 (KwaZuluNatal) on precipitation and flooding East Coast South Africa (ECSA)
ERA5 data (2-day ave) from Climexp
HighResMIP data SST forced (2-day sum) provided by Sihan Li sihan.li@ouce.ox.ac.uk
CORDEX data (2-day sum) provided by Remy Bonnet rbonnet@ipsl.fr
AMIP data (2-day ave) provided by Wenchang Yang wenchang@princeton.edu