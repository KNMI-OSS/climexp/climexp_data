#!/bin/sh
# compute land & sea separately
for type in land sea
do
    get_index giss_temp_both_1200.nc 0 360 -90 90 lsmask lsmask.nc $type > giss_$type.dat
    daily2longer giss_$type.dat 1 mean > giss_${type}_mean1.dat
    $HOME/NINO/copyfilesall.sh giss_$type.dat giss_${type}_mean1.dat
    plotdat giss_${type}_mean1.dat > giss_${type}_mean1.txt
done
plotdat giss_ts_gl_a.dat > giss.dat
