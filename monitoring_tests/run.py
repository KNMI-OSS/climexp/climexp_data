'''
Run a set of monitoring tests and raise issues on gitlab page
Sam Geen, April 2024
'''

import os, sys, time
import pytest

from io import StringIO
from contextlib import redirect_stdout

import logging
logger = logging.getLogger(__name__)

import gitlab

def runtests():
    # Return result as error code, pytest text result
    new_stdout = StringIO()
    with redirect_stdout(new_stdout):
        result = pytest.main()
    return result, new_stdout.getvalue()

def commenttestresult(comment):
    logger.info("Problem with test, raising comment:")
    logger.info(comment)
    # Get the gitlab project for climexp from the gitlab API
    token = os.environ["GITLABTOKEN"]
    gl = gitlab.Gitlab(private_token=token)
    climexp_project_id = 18251078
    project = gl.projects.get(climexp_project_id)
    # Get automatic testing issue
    issue_id = 105 
    issue = project.issues.get(issue_id)
    issue.notes.create({"body":comment})

def printresult(result):
    if result == 0:
        return "OK"
    else:
        return "Error: {0}".format(result)
    
def run():
    # Set up log file
    logfilename = "servertestlog.log"
    logging.basicConfig(filename='servertest.log', level=logging.INFO, format='%(asctime)s %(message)s')
    # Run test and log result
    logger.info("Starting new server test suite run")
    start = time.perf_counter()
    result, output = runtests()
    elapsed = time.perf_counter() - start
    logger.info("Tests completed in {elapsed:.2f} s with result {result}".format(elapsed=elapsed,result=printresult(result)))
    # If test result isn't OK, post to gitlab
    if (result != 0):
        commenttestresult(output)
        
        
if __name__=="__main__":
    run()
