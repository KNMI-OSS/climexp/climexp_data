import pytest

import os
import shutil


class TestStorage:
    """
    Tests server storage 
    """
    # bytes in 1 GiB
    GiB = 2**30
    
    def test_at_least_20GiB_on_both_drives(self):
        """Is there at least 20 GB on each drive?"""
        
        for name in ["CLIMEXP_TMP","CLIMEXP_DATA"]:
            total, used, free = shutil.disk_usage(os.environ[name])
            free /= self.GiB
            target = 20 # GiB
            assert free  >= target

    def test_climexp_data_below_150GiB(self):
        """
        Confirm that the climexp data folder (for temp user data) is below 150 GiB
        """
        path = os.environ["CLIMEXP_TMP"]+"/climexp/data/"
        used = sum(os.path.getsize(os.path.join(path,f)) for f in os.listdir(path) \
                   if os.path.isfile(os.path.join(path,f)))
        used /= self.GiB
        target = 150
        assert used <= target
