import pytest

import os
import glob
import shlex
import shutil
import subprocess

class TestClimexpNumerical:
    """
    Tests climexp numerical routines

    NOTE: Give output file names values with PYTEST in them to clear any temp data on reruns
    (I decided to not just delete any file in folders in case something dangerous happens)

    TODO: ADD OPTIONAL DATA SAMPLE TEST WHEN RESULT WRITTEN TO NETCDF OR TEXT FILE INSTEAD OF STDOUT
    """
    
    CLIMEXP_DIRECTORY = os.path.join(os.environ["CLIMEXP_TMP"], "climexp")
    TEST_CACHE_FOLDER = os.path.join(os.getcwd(), "__testcache__")
    TEST_TEMP_FOLDER = os.path.join(TEST_CACHE_FOLDER, "tmp")
    TEST_DATA_FOLDER = os.path.join(TEST_CACHE_FOLDER, "data")

    def test_daily2longer(self):
        """
        Test daily2longer
        """
        sample_result = """1951  -999.9000      -999.9000      -999.9000      -999.9000      -999.9000      -999.9000       114.8666      -999.9000      -999.9000      -999.9000      -999.9000      -999.9000"""
        self._run_numerical_test("""
        ./bin/daily2longer ./ItalyFloods2023/Italy-floods_rx21day-amj_era5.dat
        12 sum ave 2 add_persist runningmeanbackwards
        TEST_sum12_2v_persist_runningmeanbackwards
        ./data/daily2longerPYTEST_sum12_2v_persist_runningmeanbackwards.dat""",sample_result)
        
    
    def test_getmomentsfield(self):
        """
        Test getmomentsfield
        """
        test_script = """./bin/getmomentsfield UKMOData/HadCRUT.5.0.1.0.median.nc mean lsmask UKMOData/lsmask_5.nc
                      all month 1:12 ave 3 standardunits restrain 0 startstop ./tmp/startstopPYTEST.txt
                      ./data/getmomentsfieldPYTEST.nc"""
        sample_result = "Requiring at least 50% valid points<p>"
        self._run_numerical_test(test_script, sample_result)

    def test_correlatefield(self):
        """
        Test correlatefield
        """
        test_script = """./bin/correlatefield CRUData/cru_ts4.08.1901.2023.tmp.dat_25.nc 
        NCDCData/ersst_nino3a_rel.dat lsmask CRUData/lsmask_25.nc 
        all month 1 ave 3 fix2 fitfunc linear standardunits 
        startstop ./tmp/startstopPYTEST.txt ./data/PYTESTcorrelatefield.nc"""
        sample_result = "writing output"
        self._run_numerical_test(test_script, sample_result)
        
    def _run_numerical_test(self, script_text: str, sample_result: str = ""):
        """
        Run a numerical test from a line of shell script and a sample result to check for

        WARNING: DO NOT ALLOW USERS TO DEFINE script_text, USE ONLY IN TESTING

        Args:
            script_text: String to run as script in the climexp directory as a test
            sample_result: Sample result to look for in the output (optional)
        """
        self._before_each()
        # Remove unnecessary whitespace for cleaner error reporting
        script_text = " ".join(script_text.split())
        # Replace tmp and data folders with local versions
        script_text = script_text.replace("./tmp/","tmp/")
        script_text = script_text.replace(" tmp/"," "+self.TEST_TEMP_FOLDER+"/")
        script_text = script_text.replace("./data/","data/")
        script_text = script_text.replace(" data/"," "+self.TEST_DATA_FOLDER+"/")
        result = subprocess.run(script_text, capture_output=True, shell=True,
                                cwd=self.CLIMEXP_DIRECTORY)
        # No errors on exit
        if result.returncode != 0:
            print("Script return code is not zero, indicating error:", result.returncode)
            print("Script:", script_text)
            print("Stdout:", str(result.stdout, 'utf-8'))
            print("Stderr:", str(result.stderr, 'utf-8'))
            print("Expected result contains:", sample_result)
            raise ValueError
        assert result.returncode == 0
        # Sample result contained in stdout
        if len(sample_result) > 0:
            assert sample_result in str(result.stdout+result.stderr,"utf-8")

    def _before_each(self):
        self._setup_folders()
        self._make_startstop()

    def _setup_folders(self):
        os.makedirs(self.TEST_TEMP_FOLDER, exist_ok=True)
        os.makedirs(self.TEST_DATA_FOLDER, exist_ok=True)
        # Clear out existing files to prevent conflicts when running tests
        # NOTE: Be very careful TEST_DATA/TEMP_FOLDER does not have important files in it!
        for folder in[self.TEST_DATA_FOLDER,self.TEST_TEMP_FOLDER]:
            for f in glob.glob(os.path.join(folder,"*PYTEST*")):
                os.remove(f)
            
    def _make_startstop(self, filename: str = "startstopPYTEST.txt", contents: str = "1850\n2023\n"):
        with open(os.path.join(self.TEST_TEMP_FOLDER, filename), "w") as f:
            f.write("1850\n2023\n")
        
