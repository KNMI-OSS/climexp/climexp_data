import pytest

import os
import requests


class TestData:

    def test_is_cmip6_data_available(self):
        """Ensure that the CMIP6 data is available (i.e. no dead symbolic links)"""
        folder = os.environ["CLIMEXP_TMP"]+"/climexp/CMIP6/monthly_KNMI23/tas/"
        linkfile = "tas_mon_ACCESS-CM2_ssp126_000.nc"
        sourcefile = os.environ["CLIMEXP_DATA"]+"/CMIP6_KNMI23_native/tas/tas_mon_ACCESS-CM2_ssp126_000.nc"
        # Assert that file is linked correctly to the source mount
        assert os.path.realpath(os.readlink(os.path.join(folder,linkfile))) == os.path.realpath(sourcefile)
        # Assert that file contains data
        assert os.path.getsize(sourcefile) > 0


    def test_knmi_online_data_available(self):
        """
        Ensure that the KNMI online data link is available
        Tests getdutch??.cgi
        """
        url = "http://www.knmi.nl/klimatologie/daggegevens/download.html"
        response = requests.get(url)
        assert response.ok
