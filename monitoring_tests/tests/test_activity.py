import pytest

import os, re, sys
import datetime
import file_read_backwards

class TestActivity:
    """
    Tests server activity
    """
    
    
    def test_server_possible_spamming(self):
        """Has the server been noticeably overloaded at one time in the last day? 
           Test: Check that the number of processes didn't exceed 20 (may indicate spamming)
           NOTE: Can also just be a large class or similar, check logs if this is raised"""
        NPROCLIMIT = 20
        logfile = "{dir}/climexp/log/log".format(dir=os.environ['CLIMEXP_TMP'])
        today = datetime.date.today()
        yesterday = datetime.date.today() - datetime.timedelta(days=3)
        todaymatch     =     today.strftime(".*%b %d.*UTC %Y.*")
        yesterdaymatch = yesterday.strftime(".*%b %d.*UTC %Y.*")
        def isrecent(logline):
            """Is the log line from today or yesterday?"""
            if re.match(todaymatch,logline):
                return True
            if re.match(yesterdaymatch,logline):
                return True
            return False
        # Step through log lines starting from most recent
        nproc = 0
        with file_read_backwards.FileReadBackwards(logfile) as frb:
            for line in frb:
                # Step out if logline is not recent
                if not isrecent(line):
                    break
                # Check for server too busy message in line
                if not "Server too busy" in line:
                    continue
                # Grab the number of processes NN from the line 'Server too busy, load NN > 13'
                nproc = int(line[line.rfind("load")+5:line.rfind(">")])
                if nproc >= NPROCLIMIT:
                    print("Server activity exceeded normal levels:")
                    print(line)
                    break
        # Do check on number of processes
        assert(nproc < NPROCLIMIT)

