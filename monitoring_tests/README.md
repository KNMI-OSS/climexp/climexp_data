# Climate Explorer monitoring tests

## Overview

A set of monitoring tests for automatic server health scanning

Written using pytest

## Running the tests

Call:
$ python run.py

The results will appear in servertest.log. If there is a problem, it will also post to GitLab. If there are no problems, no GitLab post will appear.

## Setting up

To renew the API token, set the environment variable GITLABTOKEN.

This can be made by any Gitlab user with the ability to comment on issues. Note that if this user is you and you want to be notified of posts to the issue, you need to turn on the setting to notify yourself of your own posts. Or you can set up a dummy user for the API token. Remember to renew the token before it expires.

## Climexp Numerical tests
The climexp numerical tests runs the executables as command line scripts and checks the output

*TODO: The tests currently do not check any files created, only samples of the stdout / stderr*