Data attribution study on Drought in West Africa in 2022: 
- June averaged precipitation
- onset of rainy season
- lenth of rainy season
CHIRPS and Tamsat via Clair Barnes c.barnes22@imperial.ac.uk
MSWEP via Gerbrand Koren g.b.koren@uu.nl and Audrey Broullet audrey.brouillet@ird.fr
CORDEX and HighRESMip via Clair Barnes c.barnes22@imperial.ac.uk
FLOR and AM2.5C360 via Wenchang Yang wenchang@princeton.edu
CMIP6 via Audrey Broullet audrey.brouillet@ird.fr
