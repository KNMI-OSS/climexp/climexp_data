data for attribution analysis flooding Pakistan (Indus and Baluchistan/Sindh 2022
Observational data via Marian Zachariam m.zachariah@imperial.ac.uk
FLOR, AM2.5 via Wenchang Yang wenchang@princeton.edu
Highresmip via Mariam Zachariam and Clair Barnes m.zachariah@imperial.ac.uk / c.barnes22@imperial.ac.uk - missals were set to 3e33 before copying with Highresmip_nan.sh 
CORDEX via Robert Vautard robert.vautard@lsce.ipsl.fr, not yet provided