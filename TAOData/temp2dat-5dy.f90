program temp2dat
    implicit none
    integer,parameter :: recfa4=4
    integer,parameter :: nz=42, nlat=10, nlon=19, firstyear=1980
    integer :: ilat,ilon,i,j,k,l,m,n,n1,n2,nn,nk,yr,mn,dy,nday,ndep &
    ,nblock,iq,jdate,iqual(nz),lat,lon,kdep(nz),ldep(nz),irec &
    ,ihms,ihmsold,nmax,ii(8)
    integer :: iwrite,maxnt,ntimes,lastyear,nt
    real :: flag,depth(nz),t(nz)
    real,allocatable :: dtemp(:,:,:,:),mtemp(:,:,:),temp(:,:,:,:)
    logical :: lexist,lintdep,lintlatlon
    character infile*80, header*132, line*256
    character ns*1, ew*1, string*80, formatstring*40
    include "grid.h"
    lintdep = .true. 
    lintlatlon = .true. 
    iwrite = 1
    ihmsold = -1
    call date_and_time(values=ii)
    lastyear = ii(1)
    nt = 73*(lastyear-firstyear+1)+1
    allocate(temp(nt,nz,nlat,nlon))
    allocate(dtemp(nz,31,12,firstyear:lastyear))
    allocate(mtemp(nz,12,firstyear:lastyear))

!       loop over latitudes
    do ilat=1,nlat
        lat = lats(ilat)
    !           loop over longitudes
        do ilon=1,nlon
            lon = lons(ilon)
        
        !       Initialize t array to flag and iqual array to our undefined
        
            do k = 1,nz
                do yr=firstyear,lastyear
                    do mn=1,12
                        do dy=1,31
                            dtemp(k,dy,mn,yr) = 3e33
                        enddo
                    enddo
                enddo
            enddo
        
        !               construct filename
            if ( lat < 0 ) then
                ns='s'
            else
                ns='n'
            endif
            if ( lon < 180 ) then
                ew='e'
                i = lon
            else
                ew='w'
                i = 360-lon
            endif
            if ( i >= 100 ) then
                write(infile,'(a,i1,a,i3,a,a)') 'temp/',abs(lat),ns &
                ,i,ew,'.tmp'
            else
                write(infile,'(a,i1,a,i2,a,a)') 'temp/',abs(lat),ns &
                ,i,ew,'.tmp'
            endif
            inquire(file=infile,exist=lexist)
            if ( .not. lexist ) then
                print *,'file ',infile(1:index(infile,' ')-1) &
                ,' does not exist'
                do k=1,nz
                    do i=1,nt
                        temp(i,k,ilat,ilon) = 3e33
                    enddo
                enddo
            else
                if ( iwrite >= 0 ) print *,'Opening input file &
                ',infile(1:index(infile,' '))
                open(1,file=infile,status='old',form='formatted')
            
            !       Read total number of days, depths and blocks of data.
            
                read(1,10) nday, ndep, nblock
                10 format(49x,i7,7x,i3,8x,i3)
            
            !       Read the missing data flag
            
                read(1,20) flag
                20 format(40x,f7.3)
            
            !       Read the data
            
                do m = 1, nblock
                    read(1,30) n1, n2, nn, nk
                    30 format(50x,i8,3x,i8,x,i8,7x,i3)
                    if ( nk > nz ) then
                        write(0,*) 'error: nk = ',nk,nz
                        call exit(-1)
                    endif
                    read(1,40) (kdep(k),k=1,nk)
                    40 format(15x,1000i7)
                    do k=1,nk
                        if ( kdep(k) < 1 .or. kdep(k) > nz ) then
                            write(0,*) 'error: kdep(',k,') = ' &
                            ,kdep(k),nz
                            call exit(-1)
                        endif
                    enddo
                    read(1,'(a)') line
                    read(line(15:),*,end=901,err=901) &
                    (depth(kdep(k)),k=1,nk)
                !       Added an extra lookup-table to convert to the same depths  for
                !       all points
                    do k=1,nk
                        do l=1,nz
                            if ( depth(kdep(k)) == deps(l) ) then
                                ldep(k) = l
                            endif
                        enddo
                    enddo
                    read(1,'(a)') header
                    do n = n1, n2
                        write(formatstring,'(a,i2,a,i2,a)') &
                        '(x,i8,x,i4,x,',nk,'f7.3,x,',nk,'i1)'
                        read(1,formatstring) jdate,ihms,(t(ldep(k)) &
                        ,k=1,nk),(iqual(ldep(k)),k=1,nk)
                        yr = jdate/10000
                        mn = mod(jdate,10000)/100
                        dy = mod(jdate,100)
                        if ( ihmsold == -1 ) ihmsold = ihms
                        if ( ihms /= ihmsold ) then
                            write(0,*) 'error: hms was ',ihmsold, &
                            ' but is ',ihms
                        endif
                        ihmsold = ihms
                        if ( yr < firstyear .or. yr > lastyear ) &
                        then
                            write(0,*) 'error: yr = ',yr,firstyear &
                            ,lastyear
                            call exit(-1)
                        endif
                        if ( mn < 1 .or. mn > 12 ) then
                            write(0,*) 'error: mn = ',mn
                            call exit(-1)
                        endif
                        if ( dy < 1 .or. dy > 31 ) then
                            write(0,*) 'error: dy = ',dy
                            call exit(-1)
                        endif
                        do k=1,nk
                            if (  iqual(ldep(k)) /= 0 .and. &
                            iqual(ldep(k)) <= 3 ) then
                                dtemp(ldep(k),dy,mn,yr) = &
                                t(ldep(k))
                            endif
                        enddo
                        if ( iwrite >= 4 ) then
                            print '(i4,2i2.2,100f7.2)',yr,mn,dy &
                            ,(dtemp(k,dy,mn,yr),k=1,nz)
                        endif
                    enddo   ! n lines (days)
                enddo       ! m blocks
                close(1)
                call day2month(dtemp,mtemp(1,1,firstyear) &
                ,nz,firstyear,lastyear,0)
                call day2five(dtemp,temp(1,1,ilat,ilon),nt,maxnt &
                ,nz,firstyear,lastyear,iwrite)
                if ( iwrite >= 1 ) print *,'maxnt = ',maxnt
                if ( lintdep ) then
                    do i=1,maxnt
                        do k=1,nz
                            t(k) = temp(i,k,ilat,ilon)
                        enddo
                        call depint(t,deps,nz,0)
                        do k=1,nz
                            temp(i,k,ilat,ilon) = t(k)
                        enddo
                    enddo   ! 5-day intervals
                endif       ! depth interpolation requested
            endif           ! does file exist?
        enddo               ! lon longitudes
    enddo                   ! lat latitudes
    if ( lintlatlon ) then
        call latlonint(temp,nz*nt,nz*nt,nlat,nlon,lats,lons,iwrite, &
        -2.,35.)
        call latlonint(temp,nz*nt,nz*nt,nlat,nlon,lats,lons,iwrite, &
        -2.,35.)
    endif

!       find last year, month with data

    do nmax=73*(lastyear-firstyear+1),1,-1
        do ilat=1,nlat
            do ilon=1,nlon
                do k=1,nk
                    if ( temp(nmax,k,ilat,ilon) < 1e33 ) then
                        print *,'found first defined point at ', &
                        nmax,k,ilat,ilon,temp(nmax,k,ilat,ilon)
                        goto 100
                    endif
                enddo
            enddo
        enddo
    enddo
    100 continue
    open(1,file='tao-5dy.dat',form='unformatted',access='direct' &
    ,recl=nlon*nlat*nz*recfa4)
    irec = 0
    do n=1,nmax
        irec = irec + 1
        write(1,rec=irec) (((temp(n,k,j,i),i=1,nlon),j=1,nlat), &
        k=1,nz)
    enddo
    print *,'wrote ',irec,' records'
    close(1)
    goto 999
    901 print *,'error reading ',nk,' values from line ',line
    call exit(-1)
999 continue
end program temp2dat

