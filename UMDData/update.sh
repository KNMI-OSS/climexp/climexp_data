#!/bin/bash
# OLR
yrnow=`date -d "a month ago" +%Y`
base=https://www.ncei.noaa.gov/data/outgoing-longwave-radiation-daily/access/
version=v01r02
yr=1979
while [ $yr -le 2015 ]; do
    if [ ! -s downloaded_2015_final ]; then
        wget -q -N $base/olr-daily_${version}_${yr}0101_${yr}1231.nc
    fi
    ((yr++))
done
date > downloaded_2015_final 

ok=true
while [ $yr -lt $yrnow -a $ok = true ]; do
    wget -q -N http://olr.umd.edu/CDR/Daily/${version}/olr-daily_${version}_${yr}0101_${yr}1231.nc
    if [ ! -s olr-daily_${version}_${yr}0101_${yr}1231.nc ]; then
        ok=false
    else
        ((yr++))
    fi
done
ok=true
prelimfiles=""
base=http://olr.umd.edu/CDR/Daily/${version}-interim
while [ $yr -lt $yrnow -a $ok = true ]; do
    file=olr-daily_${version}-preliminary_${yr}0101_${yr}1231.nc
    wget -q -N $base/$file
    if [ ! -s $file ]; then
        ok=false
        echo "$0: something went wrong in retrieving http://olr.umd.edu/CDR/Daily/${version}-interim/olr-daily_${version}-preliminary_${yr}0101_${yr}1231.nc"
        exit -1
    else
        prelimfiles="$prelimfiles $file"
        ((yr++))
    fi
done

file=olr-daily_${version}-preliminary_${yrnow}0101_latest.nc
wget -N -q $base/$file
prelimfiles="$prelimfiles $file"
cdo -r -f nc4 -z zip copy olr-daily_${version}_????0101_????1231.nc $prelimfiles umd_olr_dy.nc
file=umd_olr_dy.nc
ncatted -h -a time_coverage_start,global,d,c,""  \
    -a time_coverage_end,global,d,c,""  \
    -a time_coverage_duration,global,d,c,""  $file
. $HOME/climexp/add_climexp_url_field.cgi
$HOME/NINO/copyfiles.sh umd_olr_dy.nc

version=v02r02-1
yr=`date -d "a month ago" +%Y`
mo=`date -d "a month ago" +%m`
if [ ! -s olr-monthly_${version}-1_197901_$yr$mo.nc ]; then
    wget -q -N http://olr.umd.edu/CDR/Monthly/$version/olr-monthly_${version}_197901_$yr$mo.nc
    if [ -s olr-monthly_${version}_197901_$yr$mo.nc ]; then
        cp olr-monthly_${version}_197901_$yr$mo.nc umd_olr_mo.nc
        mv olr-monthly_${version}_197901_$yr$mo.nc aap.nc
        rm -f olr-monthly_${version}_197901_??????.nc
        mv aap.nc olr-monthly_${version}_197901_$yr$mo.nc
    fi
    file=umd_olr_mo.nc
    . $HOME/climexp/add_climexp_url_field.cgi
    $HOME/NINO/copyfiles.sh umd_olr_mo.nc
fi

# compute Chiodi & Harrison (2010) OLR index
get_index umd_olr_mo.nc 170 260 -5 5 > nino_olr.dat
$HOME/NINO/copyfiles.sh nino_olr.dat

# NDVI
echo "UMD NDVI is no longer updated"
exit

mkdir -p ndvi
cd ndvi
yr=1981
mo=0712
while [ $yr -lt 2016 ]; do
	file=ndvi3g_geo_v1_${yr}_$mo.nc4
	if [ ! -s $file ]; then
		echo `date`": getting $file"
		wget -q -N https://ecocast.arc.nasa.gov/data/pub/gimms/3g.v1/$file
		# not quite CF compliant...
		ncatted -a units,time,a,c,"months since $((yr-1))-12-06" \
		    -a units,lat,a,c,'degrees_north' -a units,lon,a,c,'degrees_east' $file
	fi
	if [ ! -s $file ]; then
		echo "$0: error downloading $file, giving up"
		exit -1
	fi
	# average onto a manageable grid, combine 3x3 pixels, convert to netcdf4 with compression
	lofile=${file%.nc4}_lo.nc
	if [ ! -s $lofile -o $lofile -ot $file ]; then
		echo "cdo -r -f nc4 -z zip remapcon,logrid.txt $file $lofile"
		cdo -r -f nc4 -z zip remapcon,logrid.txt $file $lofile	
	fi
	ndvifile=${lofile%.nc}_ndvi.nc
	if [ ! -s $ndvifile ]; then
	    ncks -v ndvi $lofile tmp_$ndvifile
	    cdo monmean tmp_$ndvifile $ndvifile
	    rm tmp_$ndvifile
	fi
	if [ $mo = 0712 ]; then
	    mo=0106
        ((yr++))
	else
	    mo=0712
	fi
done
cd ..

cdo copy ndvi/*_lo_ndvi.nc gimms_ndvi.nc
export file=gimms_ndvi.nc
. $HOME/climexp/add_climexp_url_field.cgi
$HOME/NINO/copyfiles.sh gimms_ndvi.nc
