#!/bin/bash
for file in prcp_eobs*_ahrerft.dat; do
    daily2longer $file 2 max ave 1 > tmp.dat
    outfile=rx1day${file#prcp}
    fgrep '#' tmp.dat | sed -e 's/biannual/Apr-Sep/' > $outfile
    fgrep -v '#' tmp.dat | awk '{print $1 " " $3}' >> $outfile
done

for file in prcp_eobs*_meuse.dat; do
    daily2longer $file 2 max ave 2 > tmp.dat
    outfile=rx2day${file#prcp}
    fgrep '#' tmp.dat | sed -e 's/biannual/Apr-Sep/' > $outfile
    fgrep -v '#' tmp.dat | awk '{print $1 " " $3}' >> $outfile
done
