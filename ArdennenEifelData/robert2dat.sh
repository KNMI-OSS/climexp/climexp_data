#!/bin/bash
iens=0
itile=1
while [ $itile -le 14 ]; do
    for ave in 1 2; do
        for file in TILES/rrmax${ave}.tile${itile}.*.nc; do
            if [ ! -s $file ]; then
                echo "skipping $file"
            else
                ens=`printf %03i $iens`
                outfile=rx${ave}day_eurocordex_pool_${ens}.nc
                if [ ! -s $outfile ]; then
                    cdo mulc,86400 $file $outfile
                    ncatted -a units,prAdjust,a,c,"mm/dy" \
                        -a contact,global,a,c,"robert.vautard@lsce.ipsl.fr" \
                        -a institution,global,a,c,"IPSL/LSCE" \
                        $outfile
                fi
                ((iens++))
            fi
        done
    done
    ((itile++))
done

exit

for file in Upload_from_IPSL/SGSAT.????.dat; do
    ens=`basename $file .dat`
    ens=${ens#SGSAT.}
    iens=${ens#0}
    iens=${iens#0}
    iens=${iens#0}
    ((iens--))
    ens=`printf %03i $iens`
    outfile=gmst_eurocordex_gmst_${ens}.dat
    cat <<EOF > $outfile
# gmst [Celsius] smoothed global mean surface temperature anomalies
# institution :: IPSL/LSCE
# contact :: robert.vautard@lsce.ipsl.fr
EOF
    cat $file >> $outfile
done

exit

for region in ahrerft meuse geul
do
    REGION=$region
    case $region in
        meuse) REGION=MEUSE;;
        ahrerft) REGION=AHRERFT;;
        geul) REGION=SRNL1;;
    esac
    for ave in 1 2; do
        iens=0
        for file in Upload_from_IPSL/rr${ave}.${REGION}.*.nc; do
            echo "file=$file"
            if [ -s $file ]; then
                ens=`printf %03i $iens`
                outfile=prcp${ave}_eurocordex_${region}_${ens}.nc
                cdo mulc,86400 $file $outfile
                model=`basename $file`
                model=${model#rr${ave}.${REGION}.}
                model=${model%.nc}
                ncatted -h -a units,prAdjust,a,c,"mm/dy" -a model,global,a,c,$model \
                    -a contact,global,a,c,"robert.vautard@lsce.ipsl.fr" -a institution,global,a,c,"IPSL/LSCE" \
                    $outfile
                rxfile=rx${ave}day${outfile#prcp${ave}}
                rxfile=${rxfile%nc}dat
                daily2longer $outfile 2 max > tmp.dat
                fgrep '#' tmp.dat | sed -e 's/biannual/Apr-Sep/' > $rxfile
                fgrep -v '#' tmp.dat | awk '{print $1 " " $3}' >> $rxfile
                ((iens++))
            fi
        done
    done
done
