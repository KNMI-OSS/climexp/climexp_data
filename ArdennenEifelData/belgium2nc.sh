#!/bin/bash
ncrename -O -v days,time -d days,time PP.AREA.BE-MEUSE.nc tmp.nc
cdo -r -f nc4 -z zip settaxis,1951-01-01,12:00,1day tmp.nc prcp_belgiumgrid_meuse.nc
for i in 1 2; do
    ncrename -O -v years,time -d years,time RX${i}DAY.BE-MEUSE.nc tmp.nc
    cdo -r settaxis,1951-01-01,12:00,1year tmp.nc rx${i}day_belgiumgrid_meuse.nc
done
