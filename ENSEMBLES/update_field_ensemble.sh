#!/bin/bash
###set -x
cleanup=false
force=false
if [ "$1" = force ]; then
    force=true
fi
update=true
if [ "$1" = noupdate ]; then
    update=false # this means that only the official E-OBS file is downloaded, without the monthly updates
fi

# adjust with each version
version=v23.1e
# end date of official file if in the same year as the year of last month
enddate="" # "2017-08-31" # keep empty when shortening is not needed "2013-12-31"
# begin date of annual additions when in the previous year
nextdate="" # "2017-09-01" # "2016-09-01" # "2015-07-01" # keep empty when previous year is not needed

cdoflags="-r -f nc4 -z zip"
wgetflags="-N --no-check-certificate"

yr=`date -d "last month" "+%Y"`
for var in rr tg tn tx pp qq
do
  for res in 0.25 # 0.1 too big for my computers
  do

    # get pre-1950 data
    if [ $var != qq ]; then
      base=https://knmi-ecad-assets-prd.s3.amazonaws.com/ensembles/data/Grid_${res}deg_reg_ensemble
      prefile=${var}_ens_mean_${res}deg_reg_pre1950.nc
      wget $wgetflags $base/$prefile
    else
      prefile=""
    fi
 
    ###base=http://www.ecad.eu/download/ensembles/data/Grid_${res}deg_reg_ensemble/
    base=https://knmi-ecad-assets-prd.s3.amazonaws.com/ensembles/data/Grid_${res}deg_reg_ensemble
    file=${var}_ens_mean_${res}deg_reg_$version.nc
    wget $wgetflags $base/$file
    ubase=https://knmi-ecad-assets-prd.s3.amazonaws.com/ensembles/data/months/ens
    if [ -n "$nextdate" ]; then
        ufile1=${var}_${res}deg_day_$((yr-1))_grid_ensmean.nc
        wget $wgetflags $ubase/$ufile1
    fi
    ufile=${var}_${res}deg_day_${yr}_grid_ensmean.nc
    [ $update = true ] && wget $wgetflags -N $ubase/$ufile
    if [ ! -s $ufile ]; then
        echo "$0: error: something went wrong downloading $ubase/$ufile"
        exit -1
    fi
    outfile=${var}_${res}deg_reg_${version}u.nc
    if [ $force = true -o \( ! -s $outfile \) -o $ufile -nt $outfile ]; then
        if [ $update = true ]; then
            if [ -n "$nextdate" ]; then
                cdo $cdoflags seldate,$nextdate,$((yr-1))-12-31 $ufile1 ${var}_${res}deg_reg_$((yr-1)).nc
            fi
            # this gets the whole of the year
            if [ -n "$enddate" -a -z "$nextdate" ]; then
                endyr=`echo $enddate | cut -d '-' -f 1`
                endmo=`echo $enddate | cut -d '-' -f 2`
                endmo=${endmo#0}
                cdo $cdoflags seldate,${endyr}-$((endmo+1))-01,${yr}-12-31 $ufile aap.nc
            else
                endyr=$yr
                cdo $cdoflags copy $ufile aap.nc
            fi
            # truncate the part of the file without data
            get_index aap.nc 5 5 52 52 | tail -1 > aap.lastline
            yr=`cat aap.lastline | cut -b 1-4`
            if [ -z "$yr" ]; then
                echo "$0: error: cannot find date of last valid point in "`cat aap.lastline`
                exit -1
            fi
            mm=`cat aap.lastline | cut -b 5-6`
            dd=`cat aap.lastline | cut -b 7-8`
            cdo $cdoflags seldate,${endyr}-01-01,${yr}-${mm}-${dd} aap.nc ${var}_${res}deg_reg_$yr.nc

            rm -f $outfile
            if [ -n "$nextdate" ]; then
                cdo $cdoflags copy $prefile $file ${var}_${res}deg_reg_$((yr-1)).nc ${var}_${res}deg_reg_$yr.nc $outfile
                [ $? != 0 ] && echo "something went wrong" && exit -1
            else
                if [ $var = dezedoethetvandaagniet ]; then
                    echo DEBUG
                    cdo $cdoflags copy $prefile $file $outfile
                else
                    cdo $cdoflags copy $prefile $file ${var}_${res}deg_reg_$yr.nc $outfile
                fi
                [ $? != 0 ] && echo "something went wrong" && exit -1
            fi
            # set missing value to 3e33 to speed up reading in the Climate Explorer
            ###cdo setmissval,3e33 $outfile tmp$outfile  # 3e33 is not representable..
            mv tmp$outfile $outfile
            if [ $res = 0.25 ]; then # reduce size under 2^31 elements (up to 2018)
                # coordinate with extend_fields.sh
                cdo $cdoflags sellonlatbox,-26,45,30.38,72 $outfile tmp$outfile
                mv tmp$outfile $outfile
            fi
        else
            ln -s  ${var}_${res}deg_reg_${version}.nc $outfile
        fi
        if [ $var = rr ]; then
            ncatted -a units,rr,m,c,"mm/day" $outfile
        fi
        file=$outfile
        ncatted -h -a institution,global,a,c,"KNMI" -a contact,global,a,c,"eca@knmi.nl" \
                -a title,global,c,c,"E-OBS analyses $version" \
                -a source_url,global,a,c,"http://surfobs.climate.copernicus.eu//dataaccess/access_eobs.php" \
                -a References,global,a,c,"Haylock, M.R., N. Hofstra, A.M.G. Klein Tank, E.J. Klok, P.D. Jones, M. New. 2008: A European daily high-resolution gridded dataset of surface temperature and precipitation. J. Geophys. Res (Atmospheres), 113, D20119, doi:10.1029/2008JD10201" \
                $file
        . $HOME/climexp/add_climexp_url_field.cgi
        $HOME/NINO/copyfiles.sh $outfile
        if [ $res = 0.25 ]; then
            cdo $cdoflags monmean $outfile ${var}_${res}deg_reg_${version}u_mo.nc
            $HOME/NINO/copyfiles.sh ${var}_${res}deg_reg_${version}u_mo.nc
        fi
    fi
  done
done

###. ./merge_with_cru.sh
