ERA5 via Climexp 20 Dec 2022, and CPC, via Clair Barnes, c.barnes22@imperial.ac.uk
Station data via Juan Rivera jrivera@mendoza-conicet.gob.ar
UKCP18 via Clair Barnes, c.barnes22@imperial.ac.uk
HighResMIP via Clair Barnes, c.barnes22@imperial.ac.uk
FLOR and AM2.5C60 via Wenchang Yang, wenchang@princeton.edu
CMIP6 and CORDEX via Robert Vautard, rvautard@ipsl.fr