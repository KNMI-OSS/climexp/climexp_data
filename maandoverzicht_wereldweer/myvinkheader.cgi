#!/bin/sh
[ -n "$myvinkheader" ] && exit
myvinkheader=done

cat <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- beheerder: Geert Jan van Oldenborgh -->
<head>
<link rel="stylesheet" href="/styles/rijksvinkstyle.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="shortcut icon" href="http://www.knmi.nl/favicon.ico" /> 
<META HTTP-EQUIV='imagetoolbar' CONTENT='no'>
<title>KNMI - $1</title>
</head>
<body>
EOF
if [ "$FORM_lang" = nl ]; then
    algemeen=algemeen
    klimaat=Klimaat
else
    algemeen=research
    klimaat=Climate
fi
file=$climexp/vinklude/header_$algemeen.html
[ ! -s $file ] && file=../../vinklude/header_$algemeen.html
[ -s $file ] && cat $file
cat <<EOF
<table border="0" width="762" cellspacing="0" cellpadding="0">
   <tr>
      <td width="80">&nbsp;</td>
      <td width="451" valign=top>
         <div id="printable" name="printable">
         <!-- div -->
         <div class="rubriekkop">$klimaat</div>
         <div class="hoofdkop">$1</div>
         <div class="subkop">$2</div>
EOF
