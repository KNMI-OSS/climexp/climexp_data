#!/bin/sh
for file in nino34_*.eps
do
  sed -e 's/%%BoundingBox: 0 0 595 841/%%BoundingBox: 38 113 577 727/' $file > /tmp/aap.eps
  diff  $file /tmp/aap.eps
  if [ $? -ne 0 ]; then
    mv -i /tmp/aap.eps $file
  fi
done
