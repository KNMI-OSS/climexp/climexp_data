#!/bin/sh
# chop an .eps file into month;ly smaller png files
file=$1
case $file in
    icnt*) extra='/[13579]\.00) Rshow/d';;
    iN_ice*|iS_ice*|icpc_nao*) extra='/.[^0]0) Rshow/d';;
    inh_snow*) extra='/([13579]\..0. Rshow)|(.\.[^0]0. Rshow)/d';;
    *) extra='/aapnootmies/d'
esac
m=0
while [ $m -lt 12 ]
do
    m=$((m+1))
    case $m in
	1) mon=jan;;
	2) mon=feb;;
	3) mon=mar;;
	4) mon=apr;;
	5) mon=may;;
	6) mon=jun;;
	7) mon=jul;;
	8) mon=aug;;
	9) mon=sep;;
	10) mon=oct;;
	11) mon=nov;;
	12) mon=dec;;
	*) echo "chop: error: unknown month $m";exit -1;;
    esac
    x1=62
    x2=301
    y1=$((54+(12-m)*50+(13-m)/4))
    y2=$((54+(13-m)*50+(13-m)/4))
    sed -E -e "s/%%BoundingBox: 50 50 302 654/%%BoundingBox: $x1 $y1 $x2 $y2/" -e "$extra" $file > ${file%.eps}_$mon.eps
    epstopdf ${file%.eps}_$mon.eps
    gs -q -r133 -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -dNOPAUSE -sDEVICE=ppmraw -sOutputFile=-  ${file%.eps}_$mon.pdf -c quit | pnmcrop | pnmtopng > ${file%.eps}_$mon.png
    rm  ${file%.eps}_$mon.pdf
done

