#!/bin/sh
LANG=  # otherwise we get Dutch month names on the Mac
trmm_ready=false
ecmwf_ready=false

if [ "${trmm_ready}" = true ]; then
  trmm1=trmm
else
  trmm1=trmm1
fi

# compute months and dates
nextmonth=`date "+%B"`
currentmonth=`date -d "last month" "+%B"`
lastmonth=`date -d "2 months ago" "+%B"`
threemonthsago=`date -d "3 months ago" "+%B"`
sixmonthsago=`date -d "6 months ago" "+%B"`
day0=`date +%d`
mon0=`date "+%b" | tr "[:upper:]" "[:lower:]"`
mon1=`date -d "last month" "+%b" | tr "[:upper:]" "[:lower:]"`
mon2=`date -d "2 months ago" "+%b" | tr "[:upper:]" "[:lower:]"`
mon3=`date -d "3 months ago" "+%b" | tr "[:upper:]" "[:lower:]"`
mon4=`date -d "4 months ago" "+%b" | tr "[:upper:]" "[:lower:]"`
mon6=`date -d "6 months ago" "+%b" | tr "[:upper:]" "[:lower:]"`
year0=`date "+%Y"`
year1=`date -d "last month" "+%Y"`
year2=`date -d "2 months ago" "+%Y"`
year3=`date -d "3 months ago" "+%Y"`
year4=`date -d "4 months ago" "+%Y"`
year6=`date -d "6 months ago" "+%Y"`
yearp=`date -d "next month" "+%Y"`
mm0=`date "+%m" | sed -e 's/^0//'`
mm1=`date -d "last month" "+%m" | sed -e 's/^0//'`
mm2=`date -d "2 months ago" "+%m" | sed -e 's/^0//'`
mm3=`date -d "3 months ago" "+%m" | sed -e 's/^0//'`
mm4=`date -d "4 months ago" "+%m" | sed -e 's/^0//'`
mm6=`date -d "6 months ago" "+%m" | sed -e 's/^0//'`
mmp=`date -d "next month" "+%m" | sed -e 's/^0//'`
lastseason=\
`date -d "3 months ago" "+%b" | cut -b 1`\
`date -d "2 months ago" "+%b" | cut -b 1`\
`date -d "1 months ago" "+%b" | cut -b 1`
nextseason=\
`date -d "next month" "+%b" | cut -b 1`\
`date -d "2 months" "+%b" | cut -b 1`\
`date -d "3 months" "+%b" | cut -b 1`

# make figures
hostnaam=`hostname -s`
if [ $hostnaam = gatotkaca ]; then
  climexp=http://climexp.knmi.nl
else
  climexp=http://climexp.knmi.nl
fi

for var in z500 t2m sst snow ice ice_n ice_s prcp prcp_frac
do
  case $var in
  z500) field=nz500
        name=z500_ncepncar
        mproj=nps
        colour=0
        cmax=150
        lat1=
        lat2=
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  nt2m) field=nt2m
        name=t2m_ncepncar
        mproj=nps
        colour=0
        cmax=5
        lat1=
        lat2=
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  t2m)  field=ghcn_cams_05
        name=t2m_ghcncams
        mproj=nps
        colour=0
        cmax=5
        lat1=
        lat2=
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  sst)  field=sstoi_v2
        name=sst_ncep
        mproj=nps
        colour=0
        cmax=5
        lat1=
        lat2=
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  snow) field=nhsnow
        name=snow_noaa
        mproj=nps
        colour=1
        cmax=1
        lat1=
        lat2=
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  ice)  field=iceoi_v2
        name=ice_ncep
        mproj=nps
        colour=1
        cmax=1
        lat1=
        lat2=
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  ice_n) field=ice_index_n
        name=icen_nsidc
        mproj=nps
        colour=1
        cmax=1
        lat1=55
        lat2=90
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  ice_s) field=ice_index_s
        name=ices_nsidc
        mproj=sps
        colour=1
        cmax=1
        lat1=-90
        lat2=-55
        plotanomaly=on
        plotanomalykind=absolute
        climyear1=1981
        climyear2=2010
        ;;
  prcp) field=prcp_trmm_lo
        name=prcp_trmm
        mproj=latlon
        colour=1
        cmax=1
        lat1=-90
        lat2=90
        plotanomaly=
        climyear1=
        climyear2=
        ;;
  prcp_frac)
        field=prcp_trmm_lo
        name=prcp_trmm
        mproj=latlon
        colour=1
        cmax=2
        lat1=-90
        lat2=90
        plotanomaly=on
        plotanomalykind=relative
        climyear1=
        climyear2=
        ;;
  *) echo "unknown var $var";exit -1;;
  esac
  for i in 2 1 
  do
    if [ "${trmm_ready}" != true -a "${var#prcp}" != $var -a $i = 1 ]
###    if [ "${trmm_ready}" != true -a "${var#prcp}" != $var ]
    then
      field=prcp_trmm_1
      name=prcp_trmm1
    fi
    case $i in
    1) file=${name}_$mon1$year1.eps;m=$mm1;y=$year1;;
    2) file=${name}_$mon2$year2.eps;m=$mm2;y=$year2;;
    esac
    if [ $var = prcp_frac ]
    then
      file=`basename $file .eps`_frac.eps
    fi
    echo "searching for $file"
    if [ ! -s $file ]
    then
      url=`curl -s $climexp/plotfield.cgi\?EMAIL=someone@somewhere\&climyear1=$climyear1\&climyear2=$climyear2\&cmax=${cmax}\&cmin=-${cmax}\&colourscale=$colour\&field=${field}\&lat1=$lat1\&lat2=$lat2\&month=${m}\&mproj=${mproj}\&plotanomaly=$plotanomaly\&plotanomalykind=$plotanomalykind\&plotsum=1\&plottype=lat-lon\&shadingtype=shaded\&year=${y} | fgrep "eps: " | sed -e 's/.*href="//' -e 's/eps.gz.*/eps.gz/'`
      if [ -z "$url" ]; then
	echo "error computing $file"
        echo "$climexp/plotfield.cgi\?EMAIL=someone@somewhere\&climyear1=$climyear1\&climyear2=$climyear2\&cmax=${cmax}\&cmin=-${cmax}\&colourscale=$colour\&field=${field}\&lat1=$lat1\&lat2=$lat2\&month=${m}\&mproj=${mproj}\&plotanomaly=$plotanomaly\&plotanomalykind=$plotanomalykind\&plotsum=1\&plottype=lat-lon\&shadingtype=shaded\&year=${y}"
        curl -s $climexp/plotfield.cgi\?EMAIL=someone@somewhere\&climyear1=$climyear1\&climyear2=$climyear2\&cmax=${cmax}\&cmin=-${cmax}\&colourscale=$colour\&field=${field}\&lat1=$lat1\&lat2=$lat2\&month=${m}\&mproj=${mproj}\&plotanomaly=$plotanomaly\&plotanomalykind=$plotanomalykind\&plotsum=1\&plottype=lat-lon\&shadingtype=shaded\&year=${y} | sed -e '1,/Voeg hieronder de inhoud/d' -e '/Insert the body /,$d'
	exit
      fi
      echo getting $climexp/$url
      curl -s $climexp/$url > $file.gz
      gunzip $file.gz
      if [ $var = sst -o $var = ice ]; then
        mv $file $file.org
        sed -e 's/^c1 w5 c1/2000 0 translate c1 w5 c1/' $file.org > $file
      fi
      if [ $var = t2m -o $var = snow ]; then
        mv $file $file.org
        sed -e 's/^c1 w5 c1/-2000 0 translate c1 w5 c1/' $file.org > $file
      fi
    fi
  done
done

echo "searching for 50mb9065_$year0$mon0.gif"
if [ ! -s 50mb9065_$year0$mon0.gif ]; then
  curl -s http://www.cpc.ncep.noaa.gov/products/stratosphere/temperature/50mb9065.gif > 50mb9065_$year0$mon0.gif
  giftopnm 50mb9065_$year0$mon0.gif | pnmtops > 50mb9065_$year0$mon0.eps 
fi

for type in timeseries daily_concentration
do
  for h in N S
  do
    echo "searching for ${h}_${type}"
    if [ ! -s ${h}_${type}_$year0$mon0$day0.eps ]; then
      curl -s http://www.nsidc.org/data/seaice_index/images/daily_images/${h}_${type}.png > ${h}_${type}_$year0$mon0$day0.png
      pngtopnm ${h}_${type}_$year0$mon0$day0.png | pnmtops > ${h}_${type}_$year0$mon0$day0.eps
    fi
  done
done

echo "searching for prcp_gpcc_$mon1$year1.gif"
if [ ! -s prcp_gpcc_$mon1$year1.gif ]
then
  ./gpcc_gif2eps.sh $mon1$year1
fi

file=ssta_$mon1$year1.eps
echo "searching for $file"
if [ ! -s $file ]
then
  curl -s http://www.knmi.nl/waarschuwingen_en_verwachtingen/seizoensverwachting/anomaly.eps > $file
  if [ ! -s "$file" ]
  then
    echo "cannot locate $file"
    exit -1
  fi
  cmp $file ssta_$mon2$year2.eps
  if [ $? = 0 ]
  then
    rm $file
    echo "SST file seizoensverwachtingen identiek aan vorige maand"
    exit -1
  fi
fi

m=`date "+%m"`
m1=`date -d "1 month ago" "+%m"`
taolatlonfile=`ls -t lat_lon_${year0}${m}*_sst_hf*.eps | head -1`
taolatlonfileold=`ls -t lat_lon_${year1}${m1}*_sst_hf*.eps | head -1`
taodeplonfile=`ls -t dep_lon_${year0}${m}*_EQ_0_500_t_hf*.eps | head -1`
taodeplonfileold=`ls -t dep_lon_${year1}${m1}*_EQ_0_500_t_hf*.eps | head -1`
for file in "$taolatlonfile" "$taolatlonfileold" "$taodeplonfile" "$taodeplonfileold"
do
  echo "searching for $file"
  if [  -z "$file" -o  ! -s "$file" ]
  then
    echo "Please download $file"
    exit
  else
    echo found $file
  fi
done

file=tx260_$mon0$year0.eps
echo "searching for $file"
if [ ! -s $file ]
then
  curl -s http://www.knmi.nl/waarschuwingen_en_verwachtingen/maandverwachting/tx260.png > tx260_$mon0$year0.png
  ./png2eps.sh tx260_$mon0$year0.png
fi

