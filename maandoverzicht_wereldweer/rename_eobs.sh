#!/bin/sh
for var in tg tn tx rr
do
    for yr in 2010
    do
	for mon in jan feb mar apr may jun jul aug sep oct nov dec
	do
	    for ext in png txt
	    do
		[ -s ${var}_eobs_${mon}${yr}_anom.$ext ] && mv ${var}_eobs_${mon}${yr}_anom.$ext ${var}_eobs_${mon}${yr}.$ext
		[ -s ${var}_eobs_${mon}${yr}_obs.$ext ] && mv ${var}_eobs_${mon}${yr}_obs.$ext ${var}_eobs_f_${mon}${yr}.$ext
	    done
	done
    done
done
	    
