#!/bin/sh
# chop the title off a PNG file from the climexp
files="$@"
if [ -z "$files" ]; then
    files=*_[a-z][a-z][a-z][12][0-9][0-9][0-9].png
fi
for file in $files
do
    file -b $file
done
