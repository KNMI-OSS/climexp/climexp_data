#!/bin/sh
for file in "$@"
do
  pngtopnm $file | pnmcrop | pnmtops > `basename $file .png`.eps
done
