#!/bin/sh
yr=1990
while [ $yr -lt 2030 ]; do
  [ ! -d $yr ] && mkdir $yr
  mv *$yr.png *$yr.txt $yr/
  mv *${yr}_frac.png *${yr}_frac.txt $yr/
  yr=$((yr+1))
done
