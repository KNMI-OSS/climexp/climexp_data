ERA5 via Climate Explorer, Sjoukje Philip, sjoukje.philip@knmi.nl
Station data via mika.rantanen@fmi.fi (Helsinki Kaisaniemi), karianneo@met.no (Oslo Blindern), Erik.Kjellstrom@smhi.se (Abisko)
CMIP6 and CORDEX via Izidine Pinto, izidine.pinto@knmi.nl
