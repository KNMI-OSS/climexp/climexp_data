#!/bin/bash
for ifile in box/tmin_10-30E_62-70N_SCAN_*.nc; do
    cdo setmissval,-3e+33 ${ifile} ${ifile%.nc}_.nc
done
for ifile in stations/tasmin_*_Oslo.nc; do
    cdo setmissval,-3e+33 ${ifile} ${ifile%.nc}_.nc
done
