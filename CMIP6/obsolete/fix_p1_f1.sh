#!/bin/bash
# mixed up two conventions: call the p1 or f1 model run p1 or f1 when there are other values
# to emphasise it is just one of two or three choices (the old convention), or leave it out
# as superfluous (the new one). Reverting to the old one... But only the non-interpolated files
# otherwise I mess up my link farm.
# it turns out not a single model has f1 next to f2.

cd monthly
vars=`ls`
for var in $vars; do
    cd $var
    for fp in p f; do
        models=""
        for file in *-${fp}2_ssp???_???.nc; do
            model=${file#${var}_mon_}
            model=${model%%_*}
            model=${model%-${fp}2}
            models="$models $model"
        done # file
        models=`echo $models | tr ' ' '\n' | sort | uniq`
        echo "models $fp=$models"
        for model in $models; do
            for file in ${var}_mon_${model}_ssp???_???.nc; do
                if [ -s $file ]; then
                    newfile=`echo $file | sed -e "s/${model}_ssp/${model}-${fp}1_ssp/"`
                    mv $file $newfile
                fi
            done # file
            if [ $model = GISS-E2-1-G -o $model = MCM-UA-1-0 ]; then
                for file in ${var}_mon_${model}-f2_ssp???_???.nc ]; do
                    if [ -s $file -o -L $file ]; then
                        newfile=`echo $file | sed -e 's/-f2/-p1/'`
                        mv $file $newfile
                    fi
                done # file
            fi
        done # model
    done # fp
    cd ..
done # vars

