#!/bin/bash
set -e
cdo="cdo -r -f nc4 -z zip"

################################################################
# This script prepares CMIP6 data on the 192x144 regular grid: #
#                                                              #
# Remap all original CMIP6 files onto 192x144 grid.            #
#                                                              #
# Naming convention of remapped files is based on ripf variant #
################################################################

# -- Check configuration
conf_file=cmip6-data.cfg
if [ -s $conf_file ]
then
    # Use white list for security
    # Allows: comments, cmip6_*_data='a/path-t0_somewhere9', empty lines) 
    unknown=$(grep -Evi "^(#.*|cmip6_[a-z]+_data='[a-z0-9_/-]*'|)$" $conf_file)
    if [ -n "$unknown" ]; then
        echo "ERROR in config file. Not allowed lines:"
        echo $unknown
        exit 1
    fi
    
    source $conf_file
    if [[ ! -d $cmip6_original_data ]]
    then
        echo "ERROR - not a directory: $cmip6_original_data"
        exit 1
    fi
    mkdir -pv $cmip6_remapped_data
else
    echo "ERROR - empty or non-existent config file: $conf_file"
    exit 1
fi

# -- Process    
cd $cmip6_original_data
vars=`ls`

for var in $vars; do
    case $var in
        pr) interpolate="$cdo remapcon,r192x144";;
        *) interpolate="$cdo remapbil,r192x144";;
    esac

    cd $var
    models=`ls`

    for model in $models; do
        cd $model
        ripfs=`ls`

        for ripf in $ripfs; do
            cd $ripf
            for file in *.nc; do
                if [ -s $file ]; then
                    ### describefield $file
                    scenario=${file#*+}
                    scenario=${scenario%%_*}
                    string=$ripf
                    r=${string#r}
                    r=${r%%i*}
                    string=${string#r$r}
                    i=${string#i}
                    i=${i%%p*}
                    string=${string#i$i}
                    p=${string#p}
                    p=${p%%f*}
                    string=${string#p$p}
                    f=${string#f}
                    if [ ${ripf} != r${r}i${i}p${p}f${f} ]; then
                        echo "$0: error: something went wrong: $ripf != r${r}i${i}p${p}f${f}"
                    fi

                    modelpf=$model
                    if [ $p != 1 ]; then
                        modelpf=${modelpf}-p$p
                    fi
                    if [ $f != 1 -a ${model%f$f} = $model ]; then
                        modelpf=${modelpf}-f$f
                    fi

                    # Potential issues to be aware of:
                    # - r1i2 and r2i1 yield the same "ens" for the same "modelpf"
                    # - there are gaps in r for several models!

                    ens=`printf %03i $((r-1+i-1))`
                    outfile=$cmip6_remapped_data/$var/${var}_mon_${modelpf}_${scenario}_${ripf}_192_$ens.nc
                    if [ ! -s $outfile ]; then
                        echo "$interpolate $file $outfile"
                        $interpolate $file $outfile
                    fi
                else
                    echo Problem with input file: $file
                    exit 1
                fi
            done # file
            cd ..
        done # ripf
        cd ..
    done # models
    cd ..
done # vars
