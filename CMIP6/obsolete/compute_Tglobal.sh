#!/bin/bash
# gets full monthly tglobal .dat files firectly from the netcdf files
# (not through the Climate Explorer). This allows for all ensemble emmbers
# to be discovered

vars=tas
exps="ssp126 ssp245 ssp370 ssp585"
dir=$HOME/climexp/CMIP6/monthly

models=""
for var in $vars
do
    case $var in
        tos) type=mon;lats="-60 60";res=288;;
        *) type=mon;lats="-90 90";res=144;;
    esac
    for exp in $exps
    do

        # compute global means

		for file in $dir/$var/${var}_${type}_*_historical+${exp}_i1p?f?_??.nc # _${res}.nc
		do
            if [ ! -s $file ]; then
                echo "error: cannot find $file"
                exit -1
            fi
			filename=`basename $file .nc`
			m=${filename#${var}_mon_}
			model=${m%%_*}
			p2file=`(ls $dir/$var/${var}_${type}_*_historical+${exp}_i1p2f?_??.nc | head -1) 2> /dev/null`
			if [ -n "$p2file" -a -s "$p2file" ]; then
				p=${filename##*i1p}
				p=${p%.nc}
				model=${model}_p${p}
			fi
			f2file=`(ls $dir/$var/${var}_${type}_*_historical+${exp}_i1p1f2_??.nc) 2> /dev/null | head -1`
			if [ -n "$f2file" -a -s "$f2file" ]; then
				f=${filename##*i1p?f}
				f=${f%.nc}
				model=${model}_f${f}
			fi
			global=global_`basename $file .nc`
			if [ ! -s $global.dat -o $global.dat -ot $file ]; then
				echo "# <a href=http://cmip-pcmdi.llnl.gov/CMIP6/>CMIP6</a> global mean temperature computed at <a href=http://www.knmi.nl/selectfield_CMIP6.cgi?someone@somewhere>KNMI Climate Explorer</a> $filename" > $global.dat
				echo "# get_index $file 0 360 $lats >> $global.dat"
				get_index $file 0 360 $lats >> $global.dat
				dat2nc $global.dat t `basename $global .nc` $global.nc
				###rm $global.dat
			fi
		done        

        # create single-model means

        for file in global_${var}_mon_*_historical+${exp}_*_00.dat; do
            avefile=${file%_00.dat}_ave.dat
            if [ ! -s ${file%_00.dat}_01.dat ]; then
                if [ ! -L $avefile ]; then
                    ln -s $file $avefile
                    ln -s ${file%.dat}.nc ${avefile%.dat}.nc
                fi
            else
                if [ ! -s $avefile ]; then
                    average_ensemble ${file%_00.dat}_%%.dat mean > $avefile
                    dat2nc $avefile i "GMST" ${avefile%.dat}.nc
                fi 
            fi
        done

        # create model ensemble

        iens=-1
        files=`ls global_${var}_mon_*_historical+${exp}_*_ave.dat | egrep -v '_(mod|ens|one)_'`
        for file in $files; do
            ((iens++))
            ens=`printf %03i $iens`
            lfile=global_${var}_mon_mod_historical+${exp}_$ens.dat
            [ -L $lfile ] && rm $lfile
            ln -s $file $lfile
        done

        # create multimodel mean

        modfile=global_${var}_mon_mod_historical+${exp}_ave.dat
        firstfile=`ls -t $files | head -1`
        if [ ! -s $modfile -o $modfile -ot $firstfile ]; then
            average_ensemble global_${var}_mon_mod_historical+${exp}_%%%.dat mean > $modfile
            dat2nc $modfile i "GMST" ${modfile%.dat}.nc
        fi

        # create all members ensemble

        files=`ls global_${var}_mon_*_historical+${exp}_*_??.dat | egrep -v '_(mod|ens|one)_'`
        iens=-1
        for file in $files; do
            ((iens++))
            ens=`printf %03i $iens`
            lfile=global_${var}_mon_ens_historical+${exp}_$ens.dat
            [ -L $lfile ] && rm $lfile
            ln -s $file $lfile
        done

        # create one ensemble member per model ensemble

        files=`ls global_${var}_mon_*_historical+${exp}_*_00.dat | egrep -v '_(mod|ens|one)_'`
        iens=-1
        for file in $files; do
            ((iens++))
            ens=`printf %03i $iens`
            lfile=global_${var}_mon_one_historical+${exp}_$ens.dat
            [ -L $lfile ] && rm $lfile
            ln -s $file $lfile
        done

    done # exp
done
