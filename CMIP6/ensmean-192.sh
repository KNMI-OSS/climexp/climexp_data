#!/bin/bash

cdo="cdo -r -f nc4 -z zip"

# -- SWITCHES
force=false           # set to true to overwrite (recompute) model means when they exist
forceinter=false      # set to true to overwrite (recompute) intermodel means when they exist
dryrun=1              # set to 0 to execute commands, set to 1 to skip
use_prev=false        # Use means from a previous processing (link instead of compute) - applied only to intra-model means
metadata=false        # Fix metadata of inter-model means. Independent of other switches.

#####################################################################
# Create the various ensemble mean of the regridded (aka 192) files #
# for each model and across models. Use link if the mean is only    #
# over one member.                                                  #
#                                                                   #
# Based only on the available 192x144 files in $cmip6_remapped_data #
#####################################################################

# ---- UTILS

# I had to clean up some files (several models had some members
# containing an extra variable "file_qf": EC-Earth3, EC-Earth3-Veg,
# IPSL-CM6A-LR and IPSL-CM6A-LR) with this function. Done at the CLI.
cdodelit () {
    [[ $# -ne 2 ]] && { echo 'need TWO args: FILE-TO-PROCESS and VARIABLE-TO-REMOVE'; return 1; }
    mv $1 ORIG-$1
    cdo -r -f nc4 -z zip delname,$2 ORIG-$1 $1
}

# Fix metadata of inter-model means
fixmetadata () {
    local meanfile=$1
    local title=$2

    license="CMIP6 model data produced by KNMI is licensed \
under a Creative Commons Attribution 4.0 International License (CC BY \
4.0; https://creativecommons.org/licenses/). Consult \
https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing \
CMIP6 output, including citation requirements and proper \
acknowledgment. Further information about this data, including some \
limitations, can be found via the further_info_url (recorded as a \
global attribute in this file). The data producers and data providers \
make no warranty, either express or implied, including, but not \
limited to, warranties of merchantability and fitness for a particular \
purpose. All liabilities arising from the supply of the information \
(including any liability arising in negligence) are excluded to the \
fullest extent permitted by law."

    ncatted -h -a acknowledgements,global,d,, \
            -a references,global,d,, \
            -a institution,global,m,c,'KNMI' \
            -a institute_id,global,d,, \
            -a model_id,global,o,c,'multi-model (see title)' \
            -a contact,global,o,c,"climate-explorer@knmi.nl" \
            -a creation_date,global,m,c,"$(date -u +%Y-%m-%dT%H:%M:%SZ)" \
            -a title,global,o,c,"$title" \
            -a parent_experiment,global,d,, \
            -a realization,global,d,, \
            -a source,global,d,, \
            -a branch_method,global,d,, \
            -a branch_time_in_child,global,d,, \
            -a branch_time_in_parent,global,d,, \
            -a experiment,global,d,, \
            -a experiment_id,global,d,, \
            -a external_variables,global,d,, \
            -a forcing_index,global,d,, \
            -a further_info_url,global,d,, \
            -a initialization_index,global,d,, \
            -a institution_id,global,d,, \
            -a nominal_resolution,global,o,c,"217 km" \
            -a notes,global,d,, \
            -a parent_activity_id,global,d,, \
            -a parent_experiment_id,global,d,, \
            -a parent_experiment_rip,global,d,, \
            -a parent_mip_era,global,d,, \
            -a parent_source_id,global,d,, \
            -a parent_time_units,global,d,, \
            -a parent_variant_label,global,d,, \
            -a physics_index,global,d,, \
            -a realization_index,global,d,, \
            -a run_variant,global,d,, \
            -a source_id,global,d,, \
            -a source_type,global,d,, \
            -a sub_experiment,global,d,, \
            -a sub_experiment_id,global,d,, \
            -a table_info,global,d,, \
            -a variable_id,global,d,, \
            -a variant_label,global,d,, \
            -a version,global,d,, \
            -a cmor_version,global,d,, \
            -a tracking_id,global,d,, \
            -a cmip6-ng,global,d,, \
            -a license,global,o,c,"$license" \
            -a original_file_names,global,d,, \
            -a original_file_hash_codes,global,d,, \
            $meanfile
}

# -- Check configuration
conf_file=cmip6-data.cfg
if [ -s $conf_file ]
then
    # Use white list for security
    # Allows: comments, cmip6_*='/a/path-t0_somewhere9', empty lines
    unknown=$(grep -Evi "^(#.*|cmip6_[a-z_]+='/[a-z0-9_/-]*'|)$" $conf_file)
    if [ -n "$unknown" ]; then
        echo "ERROR in config file. Not allowed lines:"
        echo $unknown
        exit 1
    fi

    source $conf_file
    if [[ ! -d $cmip6_remapped_data ]]
    then
        echo "ERROR - not a directory: $cmip6_remapped_data"
        exit 1
    fi
else
    echo "ERROR - empty or non-existent config file: $conf_file"
    exit 1
fi

set -eu

# -- PROCESS
cd $cmip6_remapped_data
vars=$(ls -d */)
scenarios="ssp126 ssp245 ssp370 ssp585"

for varslash in $vars
do
    var=${varslash%/}
    [[ $var = piControl ]] && continue # skip
    cd $var
    for scenario in $scenarios
    do
        echo "=== $var $scenario ===="
        ione=0
        imod=0
        onefiles=""
        modfiles=""
        modellist=
        onemean=${var}_mon_one_${scenario}_192_ave.nc
        modmean=${var}_mon_mod_${scenario}_192_ave.nc
        ensmean=${var}_mon_ens_${scenario}_192_ave.nc

        files=`ls ${var}*_${scenario}_192_000.nc | egrep -v '_one_|_mod_|_ens_'`

        # For each model
        for file in $files
        do
            file1=${file%000.nc}001.nc
            avefile=${file%000.nc}ave.nc
            members=`echo ${file%000.nc}[0-9][0-9][0-9].nc`
            model=${file#${var}_mon_}
            model=${model%%_*}

            if [ $model = one -o $model = mod -o $model = ens ]; then
                echo " skipping $file"
            else
                echo " model $model"
                modellist="${modellist} $model"

                # Model mean
                if [[ ! -s $avefile ]] || $force ; then
                    if [ -s $file1 ]; then
                        doit=true
                        if $use_prev; then
                            PREVAVE=$cmip6_remapped_data_prev/monthly/$var/$avefile
                            if [[ ! -f $PREVAVE ]] ; then
                                echo "   WARNING missing ensmean file: $PREVAVE"
                                echo "   will ensmean original files"
                            else
                                echo "   $modelpf : model ensmean link to its previous computation"
                                (( dryrun )) || ln -sf $PREVAVE $avefile
                                doit=false
                            fi
                        fi
                        if $doit; then
                            echo "   compute $model model mean from $(echo $members | wc -w) members"
                            (( dryrun )) || $cdo ensmean $members $avefile
                        fi
                    else
                        # just one ensemble member, symlink it
                        echo "   model mean link to its one member only"
                        (( dryrun )) || ln -sf $file $avefile
                    fi
                else
                    echo "   skip model mean (existing)"
                fi

                # Collect first member of model as (model=one, member=$ione)
                one=`printf %03i $ione`
                onefile=`echo $file | sed -e "s/$model/one/" -e "s/000/$one/"`
                onefiles="$onefiles $onefile"
                echo "   link 1st member to model=one, member $one"
                (( dryrun )) || ln -sf $file $onefile
                ione=$((ione+1))

                # Collect model ensemble average as (model=mod, member=$imod)
                mod=`printf %03i $imod`
                modfile=`echo $file | sed -e "s/$model/mod/" -e "s/000/$mod/"`
                modfiles="$modfiles $modfile"
                echo "   link ensemble mean to model=mod, member $mod"
                (( dryrun )) || ln -sf $avefile $modfile
                imod=$((imod+1))

            fi # valid model
        done # models

        # Ensemble mean of all 1st members
        if [[ ! -s $onemean ]] || $forceinter
        then
            # Make sure there is no old links dangling around
            for k in $(seq $ione 999); do
                notvalid=${var}_mon_one_${scenario}_192_$(printf %03i $k).nc
                if [[ -h $notvalid ]]
                then
                    (( dryrun )) && echo "DEL: rm -f $notvalid" \
                            || rm -f $notvalid
                fi
            done
            echo " ensemble mean of all 1st members ($(echo $onefiles | wc -w) members)"
            (( dryrun )) || $cdo ensmean $onefiles $onemean
        fi

        # Ensemble mean of all model means
        if [[ ! -s $modmean ]] || $forceinter
        then
            # Make sure there is no old links dangling around
            for k in $(seq $imod 999); do
                notvalid=${var}_mon_mod_${scenario}_192_$(printf %03i $k).nc
                if [[ -h $notvalid ]]
                then
                    (( dryrun )) && echo "DEL: rm -f $notvalid" \
                            || rm -f $notvalid
                fi
            done
            echo " ensemble mean of all model means ($(echo $modfiles | wc -w) members)"
            (( dryrun )) || $cdo ensmean $modfiles $modmean
        fi

        # Ensemble mean of all members across all models
        iens=0
        ensfiles=""
        allfiles=`ls ${var}_mon_*_${scenario}_192_[0-9][0-9][0-9].nc | egrep -v '_one_|_mod_|_ens_'`

        if ! (( dryrun ))       # cleanup
        then
            for fens in ${var}_mon_ens_${scenario}_192_[0-9][0-9][0-9].nc
            do
                [[ -f $fens ]] && rm $fens
            done
        fi

        for file in $allfiles
        do
            ens=`printf %03i $iens`
            model=${file#${var}_mon_}
            model=${model%%_*}
            ensfile=`echo $file | sed -e "s/$model/ens/" -e "s/_[0-9][0-9][0-9]\.nc/_$ens.nc/"`
            (( dryrun )) || ln -sf $file $ensfile
            ensfiles="$ensfiles $ensfile"
            iens=$((iens+1))
        done

        if [[ ! -s $ensmean ]] || $forceinter
        then
            # Make sure there is no old links dangling around
            for k in $(seq $iens 999); do
                notvalid=${var}_mon_ens_${scenario}_192_$(printf %03i $k).nc
                if [[ -h $notvalid ]]
                then
                    (( dryrun )) && echo "DEL: rm -f $notvalid" \
                            || rm -f $notvalid
                fi
            done

            echo " ensemble mean of all members across all models ($(echo $ensfiles | wc -w) members)"
            (( dryrun )) || $cdo ensmean $ensfiles $ensmean
        fi

        # -- fix metadata of the three inter-model means
        if $metadata
        then
            head="Multi-model mean (mean of "
            tail=") of historical+$scenario experiments from KNMI 2023 standard set of CMIP6 models:$modellist"

            message=$head"all 1st members"$tail
            [[ -f $onemean ]] && fixmetadata $onemean "$message" || echo " MetadataFix: missing $onemean"

            message=$head"all model means"$tail
            [[ -f $modmean ]] && fixmetadata $modmean "$message" || echo " MetadataFix: missing $modmean"

            message=$head"all members across all models"$tail
            [[ -f $ensmean ]] && fixmetadata $ensmean "$message" || echo " MetadataFix: missing $ensmean"
        fi

    done # scenario
    cd ..
done # vars
