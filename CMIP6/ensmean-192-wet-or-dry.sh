#!/bin/bash

cdo="cdo -r -f nc4 -z zip"

# -- SWITCHES
dryrun=0              # set to 0 to execute commands, set to 1 to skip

#####################################################################
# Create the various ensemble mean of the regridded (aka 192) files #
# across WET and DRY models of KNMI23 standard set.                 #
#                                                                   #
# Based only on the available 192x144 files in $cmip6_remapped_data #
#####################################################################

# Util - I had to clean up some files (several models had some members
# containing an extra variable "file_qf": EC-Earth3, EC-Earth3-Veg,
# IPSL-CM6A-LR and IPSL-CM6A-LR) with this function:
cdodelit () {
    [[ $# -ne 2 ]] && { echo 'need TWO args: FILE-TO-PROCESS and VARIABLE-TO-REMOVE'; return 1; }
    mv $1 ORIG-$1
    cdo -r -f nc4 -z zip delname,$2 ORIG-$1 $1
}

# -- Check configuration
conf_file=cmip6-data.cfg
if [ -s $conf_file ]
then
    # Use white list for security
    # Allows: comments, cmip6_*='/a/path-t0_somewhere9', empty lines
    unknown=$(grep -Evi "^(#.*|cmip6_[a-z_]+='/[a-z0-9_/-]*'|)$" $conf_file)
    if [ -n "$unknown" ]; then
        echo "ERROR in config file. Not allowed lines:"
        echo $unknown
        exit 1
    fi

    source $conf_file
    if [[ ! -d $cmip6_remapped_data ]]
    then
        echo "ERROR - not a directory: $cmip6_remapped_data/piControl"
        exit 1
    fi
else
    echo "ERROR - empty or non-existent config file: $conf_file"
    exit 1
fi

set -eu

log="missing-$(date +%F).list"

# -- Process
cd $cmip6_remapped_data
vars=$(ls -d */)

# List of model and ripf id used for KNMI23 scenarios
dry='MPI-ESM1-2-LR_r10i1p1f1
    NorESM2-MM_r1i1p1f1
    UKESM1-0-LL_r1i1p1f2
    ACCESS-ESM1-5_r1i1p1f1
    MRI-ESM2-0_r1i1p1f1
    HadGEM3-GC31-LL_r1i1p1f3
    NESM3_r1i1p1f1
    BCC-CSM2-MR_r1i1p1f1
    KACE-1-0-G_r1i1p1f1
    AWI-CM-1-1-MR_r1i1p1f1
    INM-CM4-8_r1i1p1f1'

wet='CanESM5-CanOE_r1i1p2f1
    CanESM5_r10i1p1f1
    CNRM-CM6-1_r1i1p1f2
    CNRM-ESM2-1_r1i1p1f2
    CNRM-CM6-1-HR_r1i1p1f2
    IPSL-CM6A-LR_r1i1p1f1
    FGOALS-f3-L_r1i1p1f1
    EC-Earth3-Veg_r1i1p1f1
    CESM2_r4i1p1f1
    EC-Earth3_r1i1p1f1
    INM-CM5-0_r1i1p1f1'

exps="ssp126 ssp245 ssp585"     # ssp370 is not considered in KNMI23

license="CMIP6 model data produced by KNMI is licensed \
under a Creative Commons Attribution 4.0 International License (CC BY \
4.0; https://creativecommons.org/licenses/). Consult \
https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing \
CMIP6 output, including citation requirements and proper \
acknowledgment. Further information about this data, including some \
limitations, can be found via the further_info_url (recorded as a \
global attribute in this file). The data producers and data providers \
make no warranty, either express or implied, including, but not \
limited to, warranties of merchantability and fitness for a particular \
purpose. All liabilities arising from the supply of the information \
(including any liability arising in negligence) are excluded to the \
fullest extent permitted by law."


# To find the correspondence between NNN and ripf, we need to check
# the links between:
#     $cmip6_original_data/$var/$model/r*i*p${p}f${f}/*${exp}_*.nc
# and
#     $cmip6_native_data/$var/${var}_mon_${modelpf}_${exp}_$ens.nc

NNNlnk() {
    tgt=$cmip6_original_data/$var/$m/$id/*${scenario}*.nc

    # May be more than one link (case of 1-member ensembles), then
    # catch them with an array
    src=($(find -L $cmip6_native_data/$var -samefile $tgt))

    # The src## expansion returns only the first element
    (( ${#src[@]} )) && fname=$(echo ${src##*/} | sed "s|_...\.nc|_192&|") || fname=

    if [[ -n $fname ]]; then
        res=$cmip6_remapped_data/$var/$fname
        [[ -f $res ]] && echo $res
    #else
    #    echo "No file for $var $m $id $scenario" >> $log
    fi
}


for varslash in $vars
do
    var=${varslash%/}
    [[ $var =~ piControl|tasm ]] && continue # skip
    cd $var
    for scenario in $exps
    do
        for category in dry wet
        do
            echo "=== $var $scenario $category ===="
            message="Multi-model mean (one member per model) of historical+$scenario experiments of the ${category^^} models of KNMI23 standard set:"
            dmean=$cmip6_remapped_data/$var/${var}_mon_one${category}_${scenario}_192_ave.nc
            dfinal=$cmip6_final_data/$var/${var}_mon_one${category}_${scenario}_192_ave.nc
            dfiles=
            iens=0
            for mdes in ${!category}; do
                m=${mdes/_*}
                id=${mdes/*_}
                #modelpf=$m
                nnnf=$(NNNlnk)
                [[ ! -n $nnnf ]] && continue
                message="$message $m" && dfiles="$dfiles $nnnf"
                # link to member
                ens=$(printf %03i $iens)
                lnk=${dmean/ave.nc/$ens}.nc
                echo ln -s $nnnf $lnk
                [[ ! -f $lnk ]] && ln -s $nnnf $lnk   #TODO: consider a "force" option
                iens=$((iens+1))
            done

            echo -the list-
            ls -lh $dfiles          # dbg

            #echo " ensemble mean of the $category models "
            if ! (( dryrun ))
            then
                if [[ ! -f $dmean ]]; then           #TODO: consider a "force" option
                    $cdo -s ensmean $dfiles $dmean

	            # Metadata
        	    (( dryrun )) || \
                	ncatted -h -a acknowledgements,global,d,, \
                        -a references,global,d,, \
                        -a institution,global,m,c,'KNMI' \
                        -a institute_id,global,d,, \
                        -a model_id,global,o,c,'multi-model (see title)' \
                        -a contact,global,o,c,"climate-explorer@knmi.nl" \
                        -a creation_date,global,m,c,"$(date -u +%Y-%m-%dT%H:%M:%SZ)" \
                        -a title,global,o,c,"$message" \
                        -a parent_experiment,global,d,, \
                        -a realization,global,d,, \
                        -a source,global,d,, \
                        -a branch_method,global,d,, \
                        -a branch_time_in_child,global,d,, \
                        -a branch_time_in_parent,global,d,, \
                        -a experiment,global,d,, \
                        -a experiment_id,global,d,, \
                        -a external_variables,global,d,, \
                        -a forcing_index,global,d,, \
                        -a further_info_url,global,d,, \
                        -a initialization_index,global,d,, \
                        -a institution_id,global,d,, \
                        -a nominal_resolution,global,o,c,"217 km" \
                        -a notes,global,d,, \
                        -a parent_activity_id,global,d,, \
                        -a parent_experiment_id,global,d,, \
                        -a parent_experiment_rip,global,d,, \
                        -a parent_mip_era,global,d,, \
                        -a parent_source_id,global,d,, \
                        -a parent_time_units,global,d,, \
                        -a parent_variant_label,global,d,, \
                        -a physics_index,global,d,, \
                        -a realization_index,global,d,, \
                        -a run_variant,global,d,, \
                        -a source_id,global,d,, \
                        -a source_type,global,d,, \
                        -a sub_experiment,global,d,, \
                        -a sub_experiment_id,global,d,, \
                        -a table_info,global,d,, \
                        -a variable_id,global,d,, \
                        -a variant_label,global,d,, \
                        -a version,global,d,, \
                        -a cmor_version,global,d,, \
                        -a tracking_id,global,d,, \
                        -a cmip6-ng,global,d,, \
                        -a license,global,o,c,"$license" \
                        -a original_file_names,global,d,, \
                        -a original_file_hash_codes,global,d,, \
                        $dmean
                fi
            fi
            [[ ! -f $dfinal ]] && ln -s $dmean $dfinal
            ##exit  ## testrun one case
        done
    done
    cd ..
done # vars
