#!/bin/sh
# give listing of Tglobal files. To be copied into the Tglobal directory (see compute_Tglobal.sh)
echo "Content-Type: text/html"
echo

# HOME=/usr/people/oldenbor
# if [ ! -d $HOME ]; then
#     HOME=/home/oldenbor
# fi
# eval `$HOME/climexp/bin/proccgi $*`

cat <<EOF
<html>
<head>
<link rel="stylesheet" href="/styles/rccstyle.css" type="text/css">
<link rel="shortcut icon" href="/favicon.ico">
<title>Climate Explorer: CMIP6 monthly data</title>
<meta name="robots" content="noindex,nofollow">
</head>
<body>
<br><h2>Global mean temperature of CMIP6 monthly historical and future SSP experiments</h2><br>
EOF

#echo `date` "$FORM_email ($REMOTE_ADDR) CMIP6/Tglobal listing" >> $HOME/climexp/log/log
echo "<ul>"
for file in *.dat
do
    [ -f $file -a ! -L $file ] && echo "<li><a href="$file">$file</a>"
done
echo "</ul>"

echo "</body>"
echo "</html>"
