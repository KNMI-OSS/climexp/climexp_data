#!/bin/bash

#####################################################################
# Compute global means of every files in the selection of CMIP6     #
# monthly data provided by the Climate Explorer.                    #
#                                                                   #
# The global mean is written in two formats: netCDF and as an index #
# (.dat)                                                            #
#####################################################################

# -- SWITCHES
dryrun=1   # set to 0 for production, set to 1 for testing (dir and small files are still created)
debug=1

# -- Check configuration
conf_file=cmip6-data.cfg
if [ -s $conf_file ]
then
    # Use white list for security
    # Allows: comments, cmip6_*='/a/path-t0_somewhere9', empty lines
    unknown=$(grep -Evi "^(#.*|cmip6_[a-z_]+='/[a-z0-9_/-]*'|)$" $conf_file)
    if [ -n "$unknown" ]; then
        echo "ERROR in config file. Not allowed lines:"
        echo $unknown
        exit 1
    fi

    source $conf_file
    if [[ ! -d $cmip6_final_data ]]
    then
        echo "ERROR - not a directory: $cmip6_final_data"
        exit 1
    fi
else
    echo "ERROR - empty or non-existent config file: $conf_file"
    exit 1
fi

set -eu

# -- Process

vars=tas
pseudo="ens mod one"
srcdir=$cmip6_final_data
dstdir=$cmip6_tglob_data

mkdir -pv $dstdir
cp index.cgi $dstdir
cd $dstdir

for var in $vars
do
    case $var in
        tos) type=mon;lats="-60 60";;
        *) type=mon;lats="-90 90";;
    esac

    # Compute global means

    # Scan all individual members (ie "*_[0-9][0-9][0-9].nc") first.
    # Do not process those from the {ens,mod,one} pseudo-models.
    # Instead, these members are found by checking the "samelink" of
    # others, since they are designed as such in the first place.

    echo; echo " ** Individual Members **"; echo

    for file in $srcdir/$var/${var}_${type}_*_[0-9][0-9][0-9].nc
    do
        (( debug )) && echo "  ${file##*/}"
        short=$(basename $file .nc)
        global=global_$short
        model=$(echo $short | sed -n "s/${var}_${type}_\(.*\)_ssp.*/\1/p")
        (( debug )) && echo " $short: $model"

        [[ $pseudo =~ (^|[[:space:]])$model($|[[:space:]]) ]] && echo " pass" && continue

        if [[ ! -f $global.dat ]]; then
            echo "# <a href=http://cmip-pcmdi.llnl.gov/CMIP6/>CMIP6</a> global mean temperature computed at <a href=http://www.knmi.nl/selectfield_CMIP6.cgi?someone@somewhere>KNMI Climate Explorer</a> $short" > $global.dat
            echo "# get_index $file 0 360 $lats >> $global.dat"
            (( dryrun )) || get_index $file 0 360 $lats >> $global.dat
            (( dryrun )) || dat2nc $global.dat t $global $global.nc

            # Find files with same target if any:
            #  - all remapped _192 members have siblings,
            #  - all model averages that point to a single member will also be found as sibling of that member
            lnk=$(readlink -f $file)
            others=$(find -L $srcdir/$var/ -samefile $lnk)
            for fff in $others
            do
                [[ $fff = $file ]] && continue # same file
                newlnk=global_${fff##*/}
                ln -sv $global.dat ${newlnk%.nc}.dat
                (( dryrun )) || ln -s $global.nc  $newlnk
            done
        fi
    done

    echo; echo " ** Averages **"; echo

    # Scan  averages - do not skip pseudo model this time
    for file in $srcdir/$var/${var}_${type}_*_ave.nc
    do
        (( debug )) && echo "  ${file##*/}"
        short=$(basename $file .nc)
        global=global_$short

        if [[ ! -f $global.dat ]] ; then
            echo "# <a href=http://cmip-pcmdi.llnl.gov/CMIP6/>CMIP6</a> global mean temperature computed at <a href=http://www.knmi.nl/selectfield_CMIP6.cgi?someone@somewhere>KNMI Climate Explorer</a> $short" > $global.dat
            echo "# get_index $file 0 360 $lats >> $global.dat"
            (( dryrun )) || get_index $file 0 360 $lats >> $global.dat
            (( dryrun )) || dat2nc $global.dat i "GMST" $global.nc

            # Find files with same target if any:
            #  - all members of the "mod" pseudo-model will be find
            lnk=$(readlink -f $file)
            others=$(find -L $srcdir/$var/ -samefile $lnk)
            for fff in $others
            do
                [[ $fff = $file ]] && continue # same file
                newlnk=global_${fff##*/}
                ln -sv $global.dat ${newlnk%.nc}.dat
                (( dryrun )) || ln -s $global.nc  $newlnk
            done
        fi
    done

    # Test - we expect double amount of files in the destination when compared to the source dir
    nin=$(ls -1 $srcdir/$var | wc -l)
    nout=$(ls -1 $dstdir | wc -l)
    (( ! dryrun )) && nin=$((nin * 2))
    if [[ $nin -ne $nout ]]
    then
        echo "ERROR - expected number of files: $nin"
        echo "ERROR - number of files found: $nout"
        exit 1
    else
        echo "OK - created $nout files for $var"
    fi

done
