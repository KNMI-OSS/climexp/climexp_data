#!/bin/bash

cdo="cdo -r -f nc4 -z zip"

# -- SWITCHES
native=1              # set to 1 to prepare files at native resolution
remap=1               # set to 1 to remap files on a 192x144 regular grid
force=true            # set to true to overwrite (recompute) means and/or remapped when they already exist
dryrun=1              # set to 0 to execute commands, set to 1 to skip
knmi2023=1            # limit the model to process to those used in the KNMI Scenario 2023 assessment
use_prev=true         # Use means and remapped from a previous processing (link instead of compute)

#####################################################################
# This script takes the original CMIP6 data and process as follows: #
#                                                                   #
#  - Link original CMIP6 files into a staging dir (NATIVE)          #
#                                                                   #
#  - The ensemble mean for each model is also calculated (NATIVE)   #
#                                                                   #
#  - Remap all original CMIP6 files onto 192x144 regular grid in    #
#     a staging dir (REMAP)                                         #
#                                                                   #
# The name of the targets (links and remapped files) follows a      #
# naming convention that converts model/ripf into modelpf/nnn.      #
#                                                                   #
# The input and target directories are set in the cmip6-data.cfg    #
# configuration file                                                #
#####################################################################

# See README
knmi23models="ACCESS-CM2
ACCESS-ESM1-5
AWI-CM-1-1-MR
BCC-CSM2-MR
CanESM5
CanESM5-CanOE-p2
CESM2-WACCM
CESM2
CIESM
CMCC-CM2-SR5
CNRM-CM6-1-f2
CNRM-CM6-1-HR-f2
CNRM-ESM2-1-f2
EC-Earth3
EC-Earth3-Veg
FGOALS-f3-L
FGOALS-g3
GFDL-ESM4
GISS-E2-1-G-p3
HadGEM3-GC31-LL-f3
INM-CM4-8
INM-CM5-0
IPSL-CM6A-LR
KACE-1-0-G
MIROC6
MIROC-ES2L-f2
MPI-ESM1-2-HR
MPI-ESM1-2-LR
MRI-ESM2-0
NESM3
NorESM2-LM
NorESM2-MM
UKESM1-0-LL-f2"

# -- Check configuration
conf_file=cmip6-data.cfg
if [ -s $conf_file ]
then
    # Use white list for security
    # Allows: comments, cmip6_*='/a/path-t0_somewhere9', empty lines
    unknown=$(grep -Evi "^(#.*|cmip6_[a-z_]+='/[a-z0-9_/-]*'|)$" $conf_file)
    if [ -n "$unknown" ]; then
        echo "ERROR in config file. Not allowed lines:"
        echo $unknown
        exit 1
    fi

    source $conf_file
    if [[ ! -d $cmip6_original_data ]]
    then
        echo "ERROR - not a directory: $cmip6_original_data"
        exit 1
    fi

    # Target dirs
    (( dryrun )) || mkdir -pv $cmip6_native_data
    (( dryrun )) || mkdir -pv $cmip6_remapped_data
else
    echo "ERROR - empty or non-existent config file: $conf_file"
    exit 1
fi

set -eu

# -- Process
cd $cmip6_original_data
vars=`ls`
exps="ssp126 ssp245 ssp370 ssp585"

for var in $vars                # Need to start the loop on var, since not all models are available for each var
do
    case $var in
        pr) interpolate="$cdo remapcon,r192x144";;
        *) interpolate="$cdo remapbil,r192x144";;
    esac

    (( dryrun )) || mkdir -pv $cmip6_native_data/$var
    (( dryrun )) || mkdir -pv $cmip6_remapped_data/$var

    models=`(cd $cmip6_original_data/$var/; ls)`

    (( dryrun )) || cd $cmip6_native_data/$var

    for exp in $exps; do
        total_members=0
        
        for model in $models; do
            for p in 1 2 3; do
                for f in 1 2 3; do

                    modelpf=$model
                    [[ $p -gt 1 ]] && modelpf=${modelpf}-p$p
                    [[ $f -gt 1 ]] && modelpf=${modelpf}-f$f

                    echo "=== $var $exp $modelpf ===="

                    if (( knmi2023 ))
                    then
                        if [[ ! $knmi23models =~ (^|[[:space:]])$modelpf($|[[:space:]]) ]]
                        then
                            echo "  skipping $modelpf (not KNMI23 scenario)"
                            continue
                        fi
                    fi

                    iens=0
                    ens=$(printf %03i $iens)
                    linkfiles=""

                    for file in $cmip6_original_data/$var/$model/r*i*p${p}f${f}/*${exp}_*.nc; do
                        if [ -s $file ]; then

                            string=`basename $file`
                            string=${string#${var}_mon_}
                            mmodel=${string%%_*}
                            if [ $model != $mmodel ]; then
                                echo "${0##*/}: WARNING: $model /= $mmodel"
                                #exit -1
                            fi
                            string=${string#${model}_}
                            exp=${string%%_*}
                            string=${string#${exp}_} # assumes no underscore in model name!
                            exp=${exp#historical+}
                            ripf=${string%%_*}

                            linkfile=${var}_mon_${modelpf}_${exp}_$ens.nc
                            linkfiles="$linkfiles $linkfile"

                            if (( native ))
                            then
                                # 2024-08-20 - Fix CIESM pr (ESGF erratum)
                                if [[ $model = CIESM && $var = pr ]]; then
                                    echo "   cdo -mulc,1000. $file $linkfile (erratum)"
                                    (( dryrun )) || rm -f $linkfile
                                    (( dryrun )) || $cdo -mulc,1000. $file $linkfile
                                else
                                    echo "   ln -sf $file $linkfile"
                                    (( dryrun )) || ln -sf $file $linkfile
                                fi
                            fi

                            if (( remap ))
                            then
                                remapfile=$cmip6_remapped_data/$var/${var}_mon_${modelpf}_${exp}_192_$ens.nc
                                if [[ ! -s $remapfile ]] || $force
                                then
                                    doit=true
                                    if $use_prev; then
                                        PREVREMAP=$cmip6_remapped_data_prev/monthly/$var/${var}_mon_${modelpf}_${exp}_192_$ens.nc
                                        if [[ ! -f $PREVREMAP ]] ; then
                                            echo "   WARNING missing remapped file: $PREVREMAP"
                                            echo "   will interpolate original file"
                                        else
                                            echo "   ln -sf $PREVREMAP $remapfile"
                                            (( dryrun )) || ln -sf $PREVREMAP $remapfile
                                            doit=false
                                        fi
                                    fi
                                    if $doit; then
                                        # 2024-08-20 - Fix CIESM pr (ESGF erratum)
                                        [[ $model = CIESM && $var = pr ]] && interpolate="$interpolate -mulc,1000."

                                        echo "   $interpolate $file $remapfile"
                                        (( dryrun )) || $interpolate $file $remapfile
                                    fi
                                fi
                            fi

                            iens=$((iens+1))
                            ens=$(printf %03i $iens)
                        else
                            echo " no data"
                        fi
                    done

                    # Overall stat
                    total_members=$((total_members+iens))

                    # Model ensemble mean at native resolution
                    if (( native )) && [[ -n "$linkfiles" ]]
                    then
                        avefile=${var}_mon_${modelpf}_${exp}_ave.nc
                        if [[ ! -s $avefile ]] || $force
                        then
                            if [ $iens = 1 ]; then
                                echo "   $modelpf : model ensmean link to its one member only"
                                (( dryrun )) || ln -sf $linkfile $avefile
                            else
                                doit=true
                                if $use_prev; then
                                    PREVAVE=$cmip6_native_data_prev/monthly/$var/${var}_mon_${modelpf}_${exp}_ave.nc
                                    if [[ ! -f $PREVAVE ]] ; then
                                        echo "   WARNING missing ensmean file: $PREVAVE"
                                        echo "   will ensmean original files"
                                    else
                                        echo "   $modelpf : model ensmean link to its previous computation"
                                        (( dryrun )) || ln -sf $PREVAVE $avefile
                                        doit=false
                                    fi
                                fi
                                if $doit; then
                                    echo "   $modelpf : compute ensmean from $iens members"
                                    #echo "$cdo ensmean $linkfiles $avefile"
                                    (( dryrun )) || $cdo ensmean $linkfiles $avefile
                                fi
                            fi
                            echo
                        fi
                    fi

                done # f
            done # p
        done # models
        
        echo "Var=$var; Scenario=$exp; Total Members=$total_members"; echo
        
    done # exp
done # vars

