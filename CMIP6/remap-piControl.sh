#!/bin/bash

cdo="cdo -r -f nc4 -z zip"

# -- SWITCHES
force=false            # set to true to force remap (ie overwrite target if it exists)
dryrun=1              # set to 0 to execute commands, set to 1 to skip

#####################################################################
# This script takes the original piControl files and remap them.    #
#                                                                   #
#  - Only standard set of KNMI23 scenario are considered            #
#                                                                   #
#  - Remap all original CMIP6 files onto 192x144 regular grid       #
#                                                                   #
# The input ($cmip6_esgf) and target directories are set in         #
#  the cmip6-data.cfg configuration file                            #
#####################################################################
#
# Original piControl files are expected in the same dir
# for a given variable and all models:
#
#   $cmip6_esgf/${VAR}/mon/native/${VAR}_mon_${MODEL}_piControl_r1i1p1f1_native.nc
#
# with correct ripf.
#

# See README - Note that there is no piControl with f3 for the
# HadGEM3-GC31-LL model on ESGF, while there is no f1 for its
# historical/scenario runs. Basically we use f1 for piControl and f3
# for historical/scenario for that particular model. Here we list
# HadGEM3-GC31-LL in place of expected HadGEM3-GC31-LL-f3

knmi23models="ACCESS-CM2
ACCESS-ESM1-5
AWI-CM-1-1-MR
BCC-CSM2-MR
CanESM5
CanESM5-CanOE-p2
CESM2-WACCM
CESM2
CIESM
CMCC-CM2-SR5
CNRM-CM6-1-f2
CNRM-CM6-1-HR-f2
CNRM-ESM2-1-f2
EC-Earth3
EC-Earth3-Veg
FGOALS-f3-L
FGOALS-g3
GFDL-ESM4
GISS-E2-1-G-p3
HadGEM3-GC31-LL
INM-CM4-8
INM-CM5-0
IPSL-CM6A-LR
KACE-1-0-G
MIROC6
MIROC-ES2L-f2
MPI-ESM1-2-HR
MPI-ESM1-2-LR
MRI-ESM2-0
NESM3
NorESM2-LM
NorESM2-MM
UKESM1-0-LL-f2"

# -- Check configuration
conf_file=cmip6-data.cfg
if [ -s $conf_file ]
then
    # Use white list for security
    # Allows: comments, cmip6_*='/a/path-t0_somewhere9', empty lines
    unknown=$(grep -Evi "^(#.*|cmip6_[a-z_]+='/[a-z0-9_/-]*'|)$" $conf_file)
    if [ -n "$unknown" ]; then
        echo "ERROR in config file. Not allowed lines:"
        echo $unknown
        exit 1
    fi

    source $conf_file
    if [[ ! -d $cmip6_esgf ]]
    then
        echo "ERROR - not a directory: $cmip6_esgf"
        exit 1
    fi
    mkdir -pv $cmip6_tglob_data 
else
    echo "ERROR - empty or non-existent config file: $conf_file"
    exit 1
fi

set -eu

# -- Process
cd $cmip6_esgf
vars=`ls`
vars='tas'                      # dbg/process only one variable
exp=piControl

for var in $vars                # Need to start the loop on var, since not all models are available for each var
do
    case $var in
        pr) interpolate="$cdo remapcon,r192x144";;
        *) interpolate="$cdo remapbil,r192x144";;
    esac

    case $var in
        tos) type=mon;lats="-60 60";;
        *) type=mon;lats="-90 90";;
    esac

    (( dryrun )) || mkdir -pv $cmip6_remapped_data/piControl/$var

    for modelpf in $knmi23models
    do
        pf=${modelpf: -3}
        [[ $pf =~ -p([[:digit:]]) ]] && p=${BASH_REMATCH[1]} || p=1
        [[ $pf =~ -f([[:digit:]]) ]] && f=${BASH_REMATCH[1]} || f=1
        model=${modelpf%-f$f}
        model=${model%-p$p}

        echo "=== $var $model p=$p f=$f ===="

        infile=$cmip6_esgf/$var/mon/native/${var}_mon_${model}_piControl_r1i1p${p}f${f}_native.nc
        remapfile=$cmip6_remapped_data/piControl/$var/${var}_mon_${model}_${exp}_192_000.nc # one member only

        if [[ ! -f $infile ]]
        then
            echo " WARNING missing input: $infile (SKIPPING)"
        else
            if [[ ! -s $remapfile ]] || $force
            then
                # 2024-08-20 - Fix CIESM pr (ESGF erratum)
                [[ $model = CIESM && $var = pr ]] && interpolate="$interpolate -mulc,1000."

                echo "   $interpolate $infile $remapfile"
                (( dryrun )) || $interpolate $infile $remapfile
            else
                echo " do not overwrite existing target"
            fi
        fi

        # Global average of 192-remapped
        cd $cmip6_tglob_data
        short=$(basename $remapfile .nc)
        global=global_$short

        if [[ ! -f $global.nc && $var = tas ]]
        then
            (( dryrun )) || echo "# <a href=http://cmip-pcmdi.llnl.gov/CMIP6/>CMIP6</a> global mean temperature computed at <a href=http://www.knmi.nl/selectfield_CMIP6.cgi?someone@somewhere>KNMI Climate Explorer</a> $short" > $global.dat
            echo " get_index $remapfile 0 360 $lats >> $global.dat"
            (( dryrun )) || get_index $remapfile 0 360 $lats >> $global.dat
            (( dryrun )) || dat2nc $global.dat t $global $global.nc
            rm $global.dat
        fi
        cd - >/dev/null 
    done
done
