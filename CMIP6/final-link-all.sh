#! /bin/bash

# -- SWITCH
dryrun=0              # set to 0 to execute commands, set to 1 to skip

##########################################################################
# Create links to all files in the CMIP6 staging directories in the      #
# Climate Explorer CMIP6 data dir (final). Some assumptions are checked: #
# averages are first checked, then 1st member is expected.               #
##########################################################################

# -- Check configuration
conf_file=cmip6-data.cfg
if [ -s $conf_file ]
then
    # Use white list for security
    # Allows: comments, cmip6_*='/absolute/path-t0_somewhere9', empty lines
    unknown=$(grep -Evi "^(#.*|cmip6_[a-z_]+='/[a-z0-9_/-]*'|)$" $conf_file)
    if [ -n "$unknown" ]; then
        echo "ERROR in config file. Not allowed lines:"
        echo $unknown
        exit 1
    fi

    source $conf_file
    [[ ! -d $cmip6_native_data ]] && echo "ERROR - not a directory: $cmip6_native_data" && exit 1
    [[ ! -d $cmip6_remapped_data ]] && echo "ERROR - not a directory: $cmip6_remapped_data" && exit 1
else
    echo "ERROR - empty or non-existent config file: $conf_file"
    exit 1
fi

set -eu

# -- Process
cd $cmip6_native_data
vars=$(ls -d */)

for varslash in $vars
do
    var=${varslash%/}
    mkdir -pv ${cmip6_final_data}/$var

    cd ${cmip6_final_data}/$var

    for abslinkdir in ${cmip6_remapped_data}/${var} ${cmip6_native_data}/${var}
    do
        avefiles=${var}_*_ave.nc

        for linkme in $abslinkdir/$avefiles
        do
            if [ ! -f ${linkme} ]
            then
                echo "FAIL: ave-file is not a file (${linkme})"
                continue
            fi
            bname=$(basename $linkme)
            if [ ! -h $bname ]
            then
                echo " [NEW] " $bname
                (( dryrun )) || ln -s $linkme
            else
                echo " [OK] " $bname
            fi

            firstmember=${linkme%ave.nc}000.nc
            if [ ! -f $firstmember ]
            then
                echo "FAIL: 000-file does not exist (${firstmember})"
                continue
            else
                for nlinkme in ${linkme%ave.nc}[0-9][0-9][0-9].nc
                do
                    if [ ! -f $nlinkme ]
                    then
                        echo "FAIL: XXX-file is not a file (${nlinkme})"
                        continue 2
                    fi

                    bname=$(basename $nlinkme)
                    if [ ! -h $bname ]
                    then
                        echo " [NEW] " $bname
                        (( dryrun )) || ln -s $nlinkme
                    else
                        echo " [OK] " $bname
                    fi
                done
            fi
        done
    done
done
