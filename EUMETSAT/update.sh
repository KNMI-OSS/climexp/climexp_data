#!/bin/bash
#
#   fractional coud cover (CDR only)
#
ids="ORD40429 ORD40430"
for id in $ids; do
    cd $id
    firstfile=`ls CFCmm*.nc |head -1`
    if [ -z "$firstfile" -o ! -s "$firstfile" ]; then
        tar xf $id.tar
    fi
    # 198502 is missing...
    if [ ! -s CFCmm19850201000000219AVPOS01GL.nc ]; then
        cdo divc,0 CFCmm19850101000000219AVPOS01GL.nc aap.nc
        cdo settaxis,1985-02-01,0:00,1day aap.nc CFCmm19850201000000219AVPOS01GL.nc
        rm aap.nc
    fi
    for file in *.nc; do
        if [ ${file%_cfc.nc} = $file -a ! -s ${file%.nc}_cfc.nc ]; then
            echo $file
            ncks -v cfc $file ${file%.nc}_cfc.nc
            if [ ! -s ${file%.nc}_cfc.nc ]; then
                echo "$0: error: extracting cfc failed from $file"
                exit -1
            fi
        fi
    done
    cd ..
done
file=""
for id in $ids; do
    files="$files "`ls $id/CFC*_cfc.nc`
done
echo "concatenating $files"
cdo -f nc4 -z zip copy $files cfc_cmsaf.nc
ncatted -a units,cfc,m,c,"%" cfc_cmsaf.nc
ncatted -a time_coverage_start,global,d,c,"" -a time_coverage_end,global,d,c,"" \
    -a time_coverage_resolution,global,d,c,"" cfc_cmsaf.nc
export file=cfc_cmsaf.nc
. $HOME/climexp/add_climexp_url_field.cgi

#
#   Surface incoming shortwave radiation (global radiation?)
#
ids="ORD37756 ORD37757 ORD37758 ORD37759 ORD41065"
for id in $ids; do
    mkdir -p $id
    cd $id
    firstfile=`ls -t|head -1`
    if [ -n "$firstfile" -o ! -s "$firstfile" ]; then
        tar xf $id.tar
    fi
    firstfile=`ls -t *_sis.nc|head -1`
    if [ -n "$firstfile" -o ! -s "$firstfile" ]; then
        for file in *.nc; do
            if [ ${file%_sis.nc} = $file ]; then
                ncks -v SIS $file ${file%.nc}_sis.nc
                if [ ! -s ${file%.nc}_sis.nc ]; then
                    echo "$0: error: extracting sis failed from $file"
                    exit -1
                fi
                rm $file
            fi
        done
    fi
    cd ..
done
files=`echo "$ids " | sed -e 's@ @/*.nc @g'`
echo "concatenating files"
cdo -f nc4 -z zip copy $files sis_cmsaf.nc
export file=sis_cmsaf.nc
. $HOME/climexp/add_climexp_url_field.cgi
