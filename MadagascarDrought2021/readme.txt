data for attribution analysis Drought Madagascar 2021 (and 2020), precipitation 
ERA5 and CHIRPS via Climate Explorer
AMIP and FLOR data provided by Wenchang Yang wenchang@princeton.edu
HighResMIP data SST forced provided by Sihan Li sihan.li@ouce.ox.ac.uk
CORDEX data provided by Remy Bonnet rbonnet@ipsl.fr
