Data attribution study on Flooding in West Africa in 2022: 
annual 7-day max precipitation ove the Lower Niger basin and JJAS averaged precipitation over the Lake Chad basin.
CHIRPS and Tamsat via Clair Barnes c.barnes22@imperial.ac.uk
CORDEX via Clair Barnes c.barnes22@imperial.ac.uk
CMIP6 via Audrey Broullet audrey.brouillet@ird.fr
Highresmip via Mariam Zachariah m.zachariah@imperial.ac.uk 
