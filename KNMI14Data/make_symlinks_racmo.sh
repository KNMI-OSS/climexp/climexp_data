#!/bin/sh
#
# make symlinks to get an ensemble _%%.nc with %% = 00, 01, 02 , ... instead of the rN 
# (r1, r2 ,r3, ...) that is the standard.
#
# monthly data
for exp in rcp85
do
    for time in annual # mon # day
    do
        case $time in
            day) timetype=day;types=atmos/Aday;;
            mon) timetype=Amon;types=atmos/Amon;;
            annual) timetype=yr;types="year ONDJFM AMJJAS";;
            *) echo "$0: error: cannot handle time=%time yet";exit -1;;
        esac
        for type in $types
        do
            if [ $time = annual ]; then
                d=$HOME/climexp/CORDEX/WEU-11i/clim_ind
            else
                echo "not yet ready"; exit -1
            fi
            vars=`cd $d; ls`
            if [ $time = mon ]; then
                vars = "$vars pme"
            fi
            for var in $vars
            do
                echo $exp $time $type $var
                dir=CMIP5/output/KNMI/RACMO22E/$exp/$time
                [ ! -d $dir/$var ] && mkdir -p $dir/$var
                d=$HOME/climexp/CORDEX/WEU-11i/clim_ind/$var
                file=${var}_WEU-11i_KNMI-EC-EARTH_historical-${exp}_r1i1p1_KNMI-RACMO22E_v1_${type}_1950-2100.nc
                if [ ! -s $d/$file ]; then
                    echo "$0: something went wrong"
                    echo "cannot find file $d/$file"
                    exit -1
                fi
                echo $file
                i=1
                ii=00
                while [ -s $d/$file ]
                do
                    ensfile=`basename $file .nc | sed -e "s/_r${i}i1p1_/_/"`_$ii.nc
                    echo $ensfile
                    [ -f $dir/$var/$ensfile ] && rm $dir/$var/$ensfile
                    (cd $dir/$var; ln -s $d/$file $ensfile)
                    ii=`printf %02i $i`
                    i=$((i+1))
                    file=`echo $file | sed -e "s/r$((i-1))i1p1/r${i}i1p1/g"`
                    echo $file
                done
            done
        done
    done
done