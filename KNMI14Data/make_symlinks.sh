#!/bin/sh
#
# make symlinks to get an ensemble _%%.nc with %% = 00, 01, 02 , ... instead of the rN 
# (r1, r2 ,r3, ...) that is the standard.
#
# monthly data
for exp in rcp85
do
    for time in yr # mon day # yr # mon # day
    do
        case $time in
            day) timetype=day;types=atmos/Aday;;
            mon) timetype=Amon;types=atmos/Amon;;
            yr) timetype=yr;types=yr;;
            *) echo "$0: error: cannot handle time=%time yet";exit -1;;
        esac
        for type in $types
        do
            d=CMIP5/output/KNMI/ECEARTH23/$exp/$time/$type/r1i1p1/v1/
            vars=`cd $d; ls`
            if [ $time = mon ]; then
                vars = "$vars pme"
            fi
            for var in $vars
            do
                echo $exp $time $type $var
                dir=CMIP5/output/KNMI/ECEARTH23/$exp/$time/$type
                [ ! -d $dir/$var ] && mkdir $dir/$var
                if [ $var = pme ]; then
                    files=${var}/${var}_Amon_ECEARTH23_${exp}_r1i1p1_*.nc
                else
                    files=r1i1p1/v1/${var}/${var}_${timetype}_ECEARTH23_${exp}_r1i1p1_*.nc
                fi
                filelist=`ls $dir/$files | sed -e "s@$dir@@"`
                for file in $filelist; do
                    if [ ! -s $dir/$file ]; then
                        echo "$0: something went wrong"
                        echo "cannot find file $dir/$file"
                        exit -1
                    fi
                    echo $file
                    i=1
                    ii=00
                    while [ -s $dir/$file ]
                    do
                        ensfile=`basename $file .nc | sed -e "s/_r${i}i1p1_/_/"`_$ii.nc
                        echo $ensfile
                        [ -f $dir/$var/$ensfile ] && rm $dir/$var/$ensfile
                        (cd $dir/$var; ln -s ../$file $ensfile)
                        ii=`printf %02i $i`
                        i=$((i+1))
                        echo $file
                        file=`echo $file | sed -e "s/r$((i-1))i1p1/r${i}i1p1/g"`
                    done
                done
            done
        done
    done
done
