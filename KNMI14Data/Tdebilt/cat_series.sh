#!/bin/sh
iens=0
while [ $iens -lt 16 ]; do
    iens=$((iens+1))
    outfile=tas_day_ECEARTH23_rcp85_r${iens}i1p1_18600101-21001231_52N_5E.nc
    if [ ! -s $outfile ]; then
        cdo -r -f nc4 -z zip copy \
            tas_day_ECEARTH23_rcp85_r${iens}i1p1_18600101-18991231_52N_5E.nc \
            tas_day_ECEARTH23_rcp85_r${iens}i1p1_19000101-19501231_52N_5E.nc \
            tas_day_ECEARTH23_rcp85_r${iens}i1p1_19510101-20001231_52N_5E.nc \
            tas_day_ECEARTH23_rcp85_r${iens}i1p1_20010101-20501231_52N_5E.nc \
            tas_day_ECEARTH23_rcp85_r${iens}i1p1_20510101-21001231_52N_5E.nc \
            $outfile
    fi
    ens=`printf %02i $((iens-1))`
    ln -s $outfile tas_day_ECEARTH23_rcp85_${ens}_18600101-21001231_52N_5E.nc
    outfile=tas_day_ECEARTH23_rcp85_r${iens}i1p1_18600101-21001231_52N_5E.dat
    if [ ! -s $outfile ]; then
        cp tas_day_ECEARTH23_rcp85_r${iens}i1p1_18600101-18991231_52N_5E.dat $outfile
        for period in 19000101-19501231 19510101-20001231 20010101-20501231 20510101-21001231
        do
            fgrep -v '#' tas_day_ECEARTH23_rcp85_r${iens}i1p1_${period}_52N_5E.dat >> $outfile
        done
    fi
    ln -s $outfile tas_day_ECEARTH23_rcp85_${ens}_18600101-21001231_52N_5E.dat
done
