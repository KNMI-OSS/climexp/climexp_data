#!/bin/sh
# compute tas as (tasmin + tasmax/2 for the time being
i=0
ii=00
while [ -s tasmin_muladdcorr_ydrun_retrend_$ii.nc ]; do
    cdo add tasmin_muladdcorr_ydrun_retrend_$ii.nc tasmax_muladdcorr_ydrun_retrend_$ii.nc aap.nc
    cdo divc,2. aap.nc tasave_muladdcorr_ydrun_retrend_$ii.nc
    rm aap.nc
    ((i++))
    ii=`printf %02i $i`
done