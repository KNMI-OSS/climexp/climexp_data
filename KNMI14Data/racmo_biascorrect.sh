#!/bin/bash
# simple script to bias-correct RACMO data
# strategy: 
# 1 subtract trend
# 2 compute mean, variability over 1951-2000, same for E-OBS
# 3 compute correction factors by comparing tese two
# 4 apply correction factors
# 5 add trend again.

i=0
ii=00
while [ $i -lt 1 ]; do # make 16 later on
    echo ensemble $ii

    # 0 make daily global eman temperature series for RACMO    
    gmst_daily=knmi14_tas_Aday_ECEARTH23_rcp85_0-360E_-90-90N_n_su.dat
    gmst_monthly=Tglobal/iknmi14_tas_Amon_ECEARTH23_rcp85_0-360E_-90-90N_n_su_%%.dat
    gmst_annual=Tglobal/iknmi14_tas_Ayr_ECEARTH23_rcp85_0-360E_-90-90N_n_su_ave.dat
    if [ ! -s $gmst_daily ]; then
        average_ensemble $gmst_monthly mean > iknmi14_tas_Amon_ECEARTH23_rcp85_0-360E_-90-90N_n_su_ave.dat
        daily2longer iknmi14_tas_Amon_ECEARTH23_rcp85_0-360E_-90-90N_n_su_ave.dat 1 mean > $gmst_annual
        yearly2shorter $gmst_annual 366 > $gmst_daily
    fi

    for var in t2m # tasmin tasmax
    do

        # 1 compute and subtract trend
        infile=CMIP5/output/KNMI/RACMO22E/rcp85/day/atmos/$var/${var}*_$ii.nc
        correlatefield $infile $gmst_daily corr_${var}_racmo_gmst.nc
        
    done # var
    # next ensemble member
    ((i++))
    ii=`printf %02i $i`
done
