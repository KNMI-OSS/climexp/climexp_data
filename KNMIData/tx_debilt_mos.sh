#!/bin/bash
export PATH=/usr/local/bin:$PATH
if [ ! -s tx260.dat -o tx260.dat -ot tx260.gz ]; then
    gunzip -c tx260.gz > tx260.dat
fi
mv tx260.dat tx260.dat.org
fgrep '#' tx260.dat.org > tx260.dat
if [ ! -s tx260.dat ]; then
    rm -f tx260.dat
    echo "$0: tx260.dat (almost) empty, removed"
    exit -1
fi
echo "# replaced day 2-6 of ECMWF forecast by MOS corrected average of 25 and 75 percentile of 12Z forecast." >> tx260.dat
fgrep -v '#' tx260.dat.org >> tx260.dat

# For De Bilt there is even a MOS-corrected TX

wget -N http://jhot.knmi.nl/~woltersd/prob_Tx.html
if [ ! -s prob_Tx.html ]; then
    echo "$0: error: cannot find prob_Tx.html"
    exit
fi
yesterday=`date -d yesterday "+%Y%m%d"`
c=`fgrep GEBASEERD prob_Tx.html | fgrep -c $yesterday`
if [ $c = 1 ]; then
    i=0
    while [ $i -lt 6 ]; do
        ((i++))
        yyyy=`date -d "$i days" +%Y`
        mm=`date -d "$i days" +%m`
        dd=`date -d "$i days" +%d`
        line=`fgrep ${yyyy}-${mm}-${dd} prob_Tx.html | sed -e 's/&nbsp/ /g'`
        if [ -z "$line" ]; then
            echo "$0: error: cannot find ${yyyy}-${mm}-${dd} in prob_Tx.html"
        else
            range=`echo $line | cut -f 4 -d ' '`
            lo=`echo $range | cut -f 1 -d '/'`
            hi=`echo $range | cut -f 2 -d '/'`
            val=`echo "($lo + $hi)/2" | bc -l | sed -e 's/0*$//'`
            mv tx260.dat tx260.dat.bak
            sed -e "s/$yyyy$mm$dd .*$/$yyyy$mm$dd $val/" tx260.dat.bak > tx260.dat
        fi
    done
    gzip -c tx260.dat > tx260.gz
else
    echo "$0: error: prob_Tx.html not up-to-date"
fi
