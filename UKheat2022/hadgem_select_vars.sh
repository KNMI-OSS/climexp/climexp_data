#!/bin/bash
for ifile in HadGEM-GA6-N216_hist525_ens_all_mx.nc HadGEM-GA6-N216_nat525_ens_all_mx.nc HadGEM-GA6-N216_reference_ens_all_mx.nc; do
    outfile1=TXx_${ifile%.*}
    outfile2=T2dx_${ifile%.*}
	
    ncpdq -a time,realization ${ifile} ${outfile1}_tmp.nc
	cdo selname,tasmaxMax ${outfile1}_tmp.nc ${outfile1}_tmp2.nc
	cdo setmissval,-3e+33 ${outfile1}_tmp2.nc ${outfile1}_tmp3.nc
    ncrename -d realization,ens -v realization,ens ${outfile1}_tmp3.nc ${outfile1}.nc
	
    ncpdq -a time,realization ${ifile} ${outfile2}_tmp.nc
	cdo selname,tas2Max ${outfile2}_tmp.nc ${outfile2}_tmp2.nc
	cdo setmissval,-3e+33 ${outfile2}_tmp2.nc ${outfile2}_tmp3.nc
	ncrename -d realization,ens -v realization,ens ${outfile2}_tmp3.nc ${outfile2}.nc

rm *HadGEM*tmp*
done
