data for attribution analysis UK heatwave July 2022 
observations ERA5 from the Climexp (via Mariam Zachariah m.zachariah@imperial.ac.uk)
AMIP and FLOR data provided by Wenchang Yang wenchang@princeton.edu
HighResMIP data SST forced provided by Wenchang Yang wenchang@princeton.edu
CORDEX data provided by Robert Vautard robert.vautard@lsce.ipsl.fr
CMIP6 data provided by Dominik Schumacher dominik.schumacher@env.ethz.ch
HadGem3 data provided by Simon Tett Simon.Tett@ed.ac.uk
